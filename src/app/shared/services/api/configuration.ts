import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
    public Server = 'http://localhost/';
    public ApiUrl = 'api-appquimia/';
    public ImgStock = 'appquimia/imgstock/index.php'
    // public Server = 'https://appquimia.com/';
    // public ApiUrl = 'api/';
    // public ImgStock = 'admin/imgstock/index.php'
    public ServerWithApiUrl = this.Server + this.ApiUrl;
    public ServerWithImgStockUrl = this.Server + this.ImgStock;
    public SessionExpire = 900000
}
