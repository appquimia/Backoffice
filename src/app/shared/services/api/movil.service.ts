import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

// import { Configuration } from './configuration';

export class Movil {
  public id: number;
  public descripcion: string;
  public dominio: string;
  public idresponsable: string;
  public responsable: string;
  public dni: string;
  public telefono: string;
  public imagen: string;
  public nromovil: string;
  public estrellas1: string;
  public estrellas2: string;
  public estrellas3: string;
  public estrellas4: string;
  public estrellas5: string;
}

@Injectable()
export class MovilService {
  readonly GET_DETALLEMOVIL = 'd';
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {// , private configuration: Configuration) {
    // this.apiUrl = configuration.ServerWithApiUrl + 'movil';
    this.apiUrl = environment.apiUrl + 'movil';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<Movil[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Movil[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<Movil> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Movil;
          return entity;
        }
      ).catch(e => this.handleError(e));
  }

  getDetalleMovilByID(id: number): Promise<Movil> {
    const customUrl = `${this.apiUrl}/${this.GET_DETALLEMOVIL}${id}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Movil;
          return entity;
        }
      ).catch(e => this.handleError(e));
  }

  save(entity: Movil): Promise<Movil> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Movil): Promise<Movil> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Movil): Promise<Movil> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      ).catch(e => this.handleError(e));
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
