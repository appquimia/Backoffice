import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import {
    Sponsor, SponsorService,
    UploadFileService
} from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './sponsor.component.html',
    styleUrls: ['./sponsor.component.scss'],
    animations: [routerTransition()],
    providers: [
        SponsorService,
        UploadFileService]
})
export class SponsorComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Sponsor';

    public error = false;
    public loading = false;
    submitted = false;
    public selectedID: number;
    public selectedEntity: Sponsor;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modalService: NgbModal,
        private sponsorService: SponsorService,
        private uploadService: UploadFileService) {
        this.selectedEntity = new Sponsor();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.loading = true;
            this.selectedID = +params['id'];
            this.getData();
            this.loading = false;
        });
    }

    public getData() {
      if (this.selectedID > -1) {
          this.sponsorService.getByID(this.selectedID)
          .then(
              response => {
                  this.loading = false;
                  this.selectedEntity = response as Sponsor;
              })
          .catch(e => this.handleError(e));
      }
    }

    fileChange(event) {
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            this.uploadService.upload(file).then(
                response => {
                    this.selectedEntity.imagen = response.generatedName;
                }).catch(e => this.handleError(e));
        }
    }

    public volver() {
        this.router.navigate(['/sponsors']);
    }

    public guardar(content) {
        this.sponsorService
            .save(this.selectedEntity)
            .then(
                response => {
                    if (response > -1) {
                        this.loading = false;
                        this.selectedID = Number(response);
                        this.showMessage(content, 'El sponsor se guardó correctamente', this.MESSAGE_GOTO);
                    } else {
                        this.showMessage(content, 'Ya existe un sponsor con la razón social indicada!', this.MESSAGE_STAYHERE);
                        this.error = true;
                        this.loading = false;
                    }
                })
            .catch(e => this.handleError(e));
    }

    public eliminar(content) {
        this.sponsorService.delete(this.selectedEntity).then(
            response => {
              if (response) {
                  this.selectedID = -1;
                  this.showMessage(content, 'El sponsor se eliminó correctamente', this.MESSAGE_GOTO);
              } else {
                  alert('No se ha podido eliminar los datos del sponsor!!');
              }
          }).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }

    showMessage(content, message, action) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
            if (action === this.MESSAGE_GOTO) {
                this.router.navigate(['/sponsors']);
            }
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
