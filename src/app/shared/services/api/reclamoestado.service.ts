import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

// import { Configuration } from './configuration';

export class ReclamoEstado {
  public id: number;
  public estado: string;
}

@Injectable()
export class ReclamoEstadoService {
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {// , private configuration: Configuration) {
    // this.apiUrl = configuration.ServerWithApiUrl + 'reclamoestado';
    this.apiUrl = environment.apiUrl + 'reclamoestado';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<ReclamoEstado[]> {
    const options = new RequestOptions({ headers: this.headers });

    return this.http
      .get(this.apiUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as ReclamoEstado[];
        return entities;
        }
      )
      .catch(this.handleError);
  }

  getByID(id: number): Promise<ReclamoEstado> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as ReclamoEstado;
          return entity;
        }
      )
      .catch(this.handleError);
  }

  save(entity: ReclamoEstado): Promise<ReclamoEstado> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: ReclamoEstado): Promise<ReclamoEstado> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(this.handleError);
  }

  private put(entity: ReclamoEstado): Promise<ReclamoEstado> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
