import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { Ng2SmartTableModule } from 'ng2-smart-table';

import { PremiosXConductoresRoutingModule } from './premiosxconductores-routing.module';
import { PremiosXConductoresComponent } from './premiosxconductores.component';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        Ng2SmartTableModule,
        PremiosXConductoresRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [PremiosXConductoresComponent]
})
export class PremiosXConductoresModule { }
