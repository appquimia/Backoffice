import { Component, OnInit, Input, Output, EventEmitter, SimpleChange, ViewContainerRef, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    Producto, ProductoService,
    UploadFileService } from '../../../../shared/';

@Component({
    selector: 'app-producto',
    templateUrl: './producto.component.html',
    // styleUrls: ['./producto.component.scss'],
    providers: [
        ProductoService,
        UploadFileService
    ]
})

export class ProductoComponent implements OnInit, OnChanges {
    @Input() selectedItem: Producto;
    @Output() itemUpdated = new EventEmitter();

    public modalMessage: string;
    public modalTitle = 'Producto';
    // variables de campos
    private selectedIDSponsor: number;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private productoService: ProductoService,
        private uploadService: UploadFileService) {
            this.cancelar();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDSponsor = params['id'];
        });
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedItem'].currentValue !== null) {
            this.selectedItem = changes['selectedItem'].currentValue;
        }
    }

    onSubmit() {
        this.selectedItem.idsponsor = this.selectedIDSponsor;
        this.productoService.save(this.selectedItem).then(
            response => {
                if (response) {
                    this.cancelar();
                    this.itemUpdated.emit(true);
                } else {
                    this.showMessage('No se han podido guardar los datos del producto!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
      this.productoService.delete(this.selectedItem)
      .then(
          response => {
            if (response) {
                this.itemUpdated.emit(true);
                this.cancelar();
            } else {
                this.showMessage('No se han podido eliminar los datos del producto!!', false);
            }
        }).catch(e => this.handleError(e));
    }

    public cancelar() {
        this.selectedItem = new Producto();
    }

    isNew() {
      return (this.selectedItem.id === '');
    }

    fileChange(event) {
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            this.uploadService.upload(file).then(
                response => {
                    this.selectedItem.imagen = response.generatedName;
                }).catch(e => this.handleError(e));
        }
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/sponsors']);
                        } else {
                            this.router.navigate(['/sponsors', this.selectedIDSponsor]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
