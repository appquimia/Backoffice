import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import {
    NgbDateCustomParserFormatter,
    SponsorService, PremiosLista, ReporteService,
    GenPremiosXConductor, TWUtils } from '../../shared';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

@Component({
    selector: 'app-tables',
    templateUrl: './reportepremiosxsponsor.component.html',
    styleUrls: ['./reportepremiosxsponsor.component.scss'],
    animations: [routerTransition()],
    providers: [
        DatePipe,
        ReporteService,
        SponsorService,
        {provide: LOCALE_ID, useValue: 'es-AR'},
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}
    ]
})

export class ReportePremiosXSponsorComponent implements OnInit {
    public modalTitle = 'Premios por sponsor';
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            codigopremio: { title: 'Código', filter: false, },
            premio: { title: 'Descripción', filter: false, },
            cantidad: { title: 'Cantidad', filter: false, },
            canjeado: { title: 'Canjeados', filter: false, },
            canjeadocond:{ title: 'Canajeado x conductores', filter: false, },
            imagen: {
                title: 'Imagen', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<img src="' + value + '" alt="Smiley face" height="75" width="75">';
                }
            }
         },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;

    public selectedEntity: PremiosLista;
    public fecInicio: {};
    public fecFin: {};
    public sponsorsFilt: number;
    public error = false;
    public loading = false;
    public data: Array<any> = Array<any>();
    public dataSponsors: Array<any> = Array<any>();

    public constructor(public router: Router,
        private dp: DatePipe,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private sponsorService: SponsorService,
        private reporteService: ReporteService) {
        this.sponsorsFilt = -1;
        this.fecInicio = {};
        this.fecFin = {};
    }

    public ngOnInit(): void {
        this.getData();
    }

    public getData() {
        const p0 = this.sponsorService.getList();
        Promise.all([
            p0
        ]).then(([result0]) => {
            this.dataSponsors = result0;
        }).catch(e => this.handleError(e));
    }

    public filtrar() {
        const datePipe = new DatePipe('en-US');
        const fechamin = TWUtils.arrayDateToString(this.fecInicio);
        const fechamax = TWUtils.arrayDateToString(this.fecFin); 
        if (fechamin !== -1 && fechamax !== -1 && this.sponsorsFilt !== undefined) {
            this.reporteService.getPremiosXSponsor(this.sponsorsFilt, fechamin, fechamax).then(
                response => {
                    if (response) {
                        this.loading = false;
                        this.data = response;
                        this.source = new LocalDataSource(this.data);
                    } else {
                        this.showMessage('No se han podido cargar datos de premios por sponsor!!', false, false);
                        this.loading = false;
                    }
                }
            ).catch(
                e => this.handleError(e));
        } else { console.log('Error')}
    }

    export() {
        const fechamin = TWUtils.arrayDateToStringFormated(this.fecInicio);
        const fechamax = TWUtils.arrayDateToStringFormated(this.fecFin);
        const fecreporte = this.dp.transform(Date.now(), 'dd-MM-yyyy');
        if (fechamin !== -1 && fechamax !== -1 && this.sponsorsFilt !== undefined) {
            const sponsor = this.dataSponsors.find(data => +data.id === +this.sponsorsFilt);
            const voucher = new GenPremiosXConductor();
            voucher.genPdf(sponsor, fecreporte, fechamin, fechamax, this.data);
        }
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'codigo', search: query, },
            { field: 'descripcion', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        const fechamin = TWUtils.arrayDateToString(this.fecInicio);
        const fechamax = TWUtils.arrayDateToString(this.fecFin);
        localStorage.setItem('fecinicio', fechamin);
        localStorage.setItem('fecfin', fechamax);
        this.router.navigate(['/reportepremioxsponsor', event.data.idpremio]);
    }

    showMessage(message: string, goBack: boolean, notAuth: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (notAuth) {
                            this.router.navigate(['/dashboard']);
                        }
                        if (goBack) {
                            this.router.navigate(['/dashboard']);
                        } else {
                            this.router.navigate(['/dashboard']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
