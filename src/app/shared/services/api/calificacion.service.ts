import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

// import { Configuration } from './configuration';

export class Calificacion {
  public id: string;
  public idcuenta: string;
  public nombre: string;
  public estrellasconductor: number;
  public fecha: string;
  public estrellasmovil: number;
  public idmovil: string;
  public idconductor: string;
  public dominio: string;
  public descripcion: string;
}

export class CalificacionResumen {
  public idmovil: string;
  public idconductor: string;
  public conductor: string;
  public nromovil: number;
  public dominio: string;
  public imagen: string;
  public movil1: number;
  public movil2: number;
  public movil3: number;
  public movil4: number;
  public movil5: number;
  public conductor1: number;
  public conductor2: number;
  public conductor3: number;
  public conductor4: number;
  public conductor5: number;
  public responsable: string;
  public telefono: string;
}

export class CalificacionLista {
  public idmovil: string;
  public idconductor: string;
  public nromovil: string;
  public conductor: string;
  public estrellasmovil: number;
  public estrellasconductor: number;
}

@Injectable()
export class CalificacionService {
  readonly PATCH_LISTFILTRADA = 'f';
  readonly PATCH_RESUMEN = 'r';
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {// , private configuration: Configuration) {
    // this.apiUrl = configuration.ServerWithApiUrl + 'calificacion';
    this.apiUrl = environment.apiUrl + 'calificacion';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<Calificacion[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Calificacion[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }

  getListFiltered(estrellas: number, fecmin: string, fecmax: string): Promise<CalificacionLista[]> {
    const customUrl = `${this.apiUrl}/${this.PATCH_LISTFILTRADA}`;
    const obj = { 'fecmin': fecmin, 'fecmax': fecmax, 'estrellas': estrellas};

    return this.http
      .patch(customUrl, JSON.stringify(obj), { headers: this.headers })
      .toPromise()
      .then((response: Response) => {
        const body = response.json();
        const entities = body || {} as CalificacionLista[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }

  getByIDs(idmovil: string, idconductor: string): Promise<CalificacionResumen> {
    const customUrl = `${this.apiUrl}/${this.PATCH_RESUMEN}`;
    const obj = { 'idmovil': idmovil, 'idconductor': idconductor};
    return this.http
      .patch(customUrl, JSON.stringify(obj), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as CalificacionResumen;
          return entity;
        }
      ).catch(e => this.handleError(e));
  }

  save(entity: Calificacion): Promise<Calificacion> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Calificacion): Promise<Calificacion> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Calificacion): Promise<Calificacion> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      ).catch(e => this.handleError(e));
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
