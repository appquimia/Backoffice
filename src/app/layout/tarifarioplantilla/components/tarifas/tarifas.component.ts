import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';

import { TarifaPlantilla, TarifaPlantillaService } from '../../../../shared/services';

@Component({
    selector: 'app-tarifas',
    templateUrl: './tarifas.component.html',
    providers: [
        TarifaPlantillaService
    ]
})

export class TarifasComponent implements OnInit {
    @Output() itemsUpdated = new EventEmitter();

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
          fecha: { title: 'Fecha', filter: false, },
          kms: { title: 'Kilómetros', filter: false, },
          valor: { title: 'Valor', filter: false, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;

    public dataEstados: Array<any> = Array<any>();
    public selectedItem: TarifaPlantilla;
    public error = false;
    public loading = false;
    public data: Array<any> = Array<any>();

    public constructor(public router: Router,
        private tarifaService: TarifaPlantillaService) {
        this.selectedItem = new TarifaPlantilla();
        this.data = [];
        this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void  {
        this.getData();
    }

    getData() {
        this.tarifaService.getTarifas().then(
            response => {
                if (response) {
                    this.loading = false;
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                } else {
                    this.error = true;
                    this.loading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    public userRowSelect(event: any): any {
        this.selectedItem = event.data;
    }

    handleItemUpdated() {
      this.getData();
      this.itemsUpdated.emit(true);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            {
                field: 'valor',
                search: query,
            },
            {
                field: 'kms',
                search: query,
            }
        ], false);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
