import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportePremiosXSponsorComponent } from './reportepremiosxsponsor.component';

const routes: Routes = [
    { path: '', component: ReportePremiosXSponsorComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ReportePremiosXSponsorRoutingModule { }
