import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

// import { Configuration } from './configuration';

/*
  IMPORTANTE: esta api se basa sobre la tabla premiosxcuentas
    pero se utiliza para premios por conductores
*/
export class Reporte {
  public id: string;
  public idpremio: number;
  public premio: string;
  public idsponsor: string;
  public sponsor: string;
  public idcuenta: string;
  public cuenta: string;
  public fecpremio: string;
  public fecven: string;
  public idestado: number;
  public estado: string;
  public idmovil: string;
  public nromovil: string;
  public movil: string;
  public idconductor: string;
  public conductor: string;
  public idbitacora: string;
  public idestadoprexcond: number;
  public estadoprexcond: string;
  public feccanjeo: string;
  public feccanjeocond: string;
  public codigopremio: string;
  public direccion: string;
  public selected: number;
  public cantidad: number;
  public canjeado: number;
  public canjeadocond: number;

  constructor() {
    this.selected = 0;
  }
}

export class PremiosLista {
  public idpremio: string;
  public idsponsor: string;
  public codigo: string;
  public razonsocial: string;
  public descripcion: string;
  public nombre: string;
  public email: string;
  public fechapremio: string;
  public canjeado: number;
  public canjeadocond: number;
}

@Injectable()
export class ReporteService {
  readonly GET_BYID = 'byid';
  readonly GET_RESUMENXESTADO = 'resumenxestado';
  readonly GET_BYCONDUCTORXMOVIL = 'byconductorxmovil';
  readonly PUT_CANJEOCOND = 'canjeocond';
  readonly PATCH_PREMIOSXFECHA = 'premiosxfecha';
  readonly PATCH_PREMIOXFECHA = 'premioxfecha';

  private apiUrl = '';
  private headers;

  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'premioxconductor';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<Reporte[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, this.headers)
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Reporte[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getPremiosXSponsor(idsponsor: number, fecmin: string, fecmax: string): Promise<PremiosLista[]> {
    const customUrl = `${this.apiUrl}/${this.PATCH_PREMIOSXFECHA}`;
    const obj = { 'idsponsor': idsponsor, 'fecmin': fecmin, 'fecmax': fecmax};

    return this.http
      .patch(customUrl, JSON.stringify(obj), { headers: this.headers })
      .toPromise()
      .then((response: Response) => {
        const body = response.json();
        const entities = body || {} as PremiosLista[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }

  getPremioXFecha(idpremio, fecmin: string, fecmax: string): Promise<Reporte[]> {
    const customUrl = `${this.apiUrl}/${this.PATCH_PREMIOXFECHA}`;
    const obj = { 'idpremio': idpremio, 'fecmin': fecmin, 'fecmax': fecmax};

    return this.http
      .patch(customUrl, JSON.stringify(obj), { headers: this.headers })
      .toPromise()
      .then((response: Response) => {
        const body = response.json();
        const entities = body || {} as PremiosLista[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }
  /*
  SECCION PREMIOS X CONDUCTOR
  */
  getResumenXEstado(idestado: number): Promise<Reporte[]> {
    const customUrl = `${this.apiUrl}/${this.GET_RESUMENXESTADO}/${idestado}`;

    return this.http
      .get(customUrl, this.headers)
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Reporte[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }

  getByID(id: string): Promise<Reporte> {
    const customUrl = `${this.apiUrl}/${this.GET_BYID}/${id}`;

    return this.http
      .get(customUrl, this.headers)
      .toPromise()
      .then((response: Response) => {
          const body = response.json();
          const entity = body[0] as Reporte;
          return entity;
        }
      ).catch(e => this.handleError(e));
  }

  getByIDConductorIDMovil(id: number): Promise<Reporte[]>  {
    const customUrl = `${this.apiUrl}/${this.GET_BYCONDUCTORXMOVIL}/${id}`;

    return this.http
    .get(customUrl, this.headers)
    .toPromise()
    .then(response => {
      const body = response.json();
      const entities = body || {} as Reporte[];
      return entities;
      }
    ).catch(e => this.handleError(e));
  }

  canjear(entity: Reporte): Promise<Reporte> {
    const url = `${this.apiUrl}/${this.PUT_CANJEOCOND}`;

    return this.http
      .put(url, JSON.stringify(entity))
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      ).catch(e => this.handleError(e));
  }

  save(entity: Reporte): Promise<Reporte> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Reporte): Promise<Reporte> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity))
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Reporte): Promise<Reporte> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity))
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      ).catch(e => this.handleError(e));
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
