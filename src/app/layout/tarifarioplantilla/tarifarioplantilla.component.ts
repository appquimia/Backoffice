import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { TarifaPlantillaService } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './tarifarioplantilla.component.html',
    styleUrls: ['./tarifarioplantilla.component.scss'],
    animations: [routerTransition()],
    providers: [
        NgbModal,
        TarifaPlantillaService ]
})

export class TarifarioPlantillaComponent implements OnInit {readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Plantilla de tarifario';

    public constructor(
        public router: Router,
        private viewContainer: ViewContainerRef,
        private modalService: NgbModal,
        private tarifaPlantillaService: TarifaPlantillaService) {
    }

    public ngOnInit(): void {
    }

    aplicar(content) {
        this.tarifaPlantillaService.aplicar()
            .then(
                response =>{
                    if (response) {
                        console.log('ok')
                        this.showMessage(content, 'Se aplicó la plantilla correctamente!!');
                    }
                })
            .catch(e => console.log('error'));
    }

    showMessage(content, message) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }
}
