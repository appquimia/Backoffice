import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class Sucursal {
    public id: string;
    public idsponsor: number;
    public sucursal: string;
    public direccion: string;
    public descripcion: string;
    public estrellasatencion: number;
    public estrellaslocal: number;

    constructor() {
        this.id = '';
    }
}

@Injectable()
export class SucursalService {
    readonly GET_BYIDSPONSOR = 'byidsponsor';

    private apiUrl = '';
    private headers;
    constructor(private http: Http) {
        this.apiUrl = environment.apiUrl + 'sucursal';
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Cache-Control', 'no-cache');
        this.headers.append('Pragma', 'no-cache');
    }

    getList(): Promise<Sucursal[]> {
        const customUrl = `${this.apiUrl}`;

        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as Sucursal[];
                return entities;
            }
            )
            .catch(e => this.handleError(e));
    }

    getListByIdSponsor(idsponsor: number): Promise<Sucursal[]> {
        const customUrl = `${this.apiUrl}/${this.GET_BYIDSPONSOR}/${idsponsor}`;

        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as Sucursal[];
                return entities;
            }
            )
            .catch(e => this.handleError(e));
    }

    getByID(id: string): Promise<Sucursal> {
        return this.http
            .get(this.apiUrl + '/' + id, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body[0] as Sucursal;
                return entity;
            }
            )
            .catch(e => this.handleError(e));
    }

    save(entity: Sucursal): Promise<Sucursal> {
        if (entity.id.length > 0) {
            return this.put(entity);
        }
        return this.post(entity);
    }

    private post(entity: Sucursal): Promise<Sucursal> {
        return this.http
            .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                return body.status === 'success';
            }
            )
            .catch(e => this.handleError(e));
    }

    private put(entity: Sucursal): Promise<Sucursal> {
        const url = `${this.apiUrl}/${entity.id}`;

        return this.http
            .put(url, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                return body.status === 'success';
            }
            )
            .catch(e => this.handleError(e));
    }

    delete(entity: Sucursal): Promise<Response> {
        const customUrl = `${this.apiUrl}/${entity.id}`;

        return this.http
            .delete(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                if (response.status === 204) {
                    return response.ok;
                } else if (response.status === 202) {
                    return -1;
                }
            })
            .catch(
                error =>
                    this.handleError(error)
            );
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
