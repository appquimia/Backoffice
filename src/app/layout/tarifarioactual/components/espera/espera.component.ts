import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import {Espera, EsperaService} from '../../../../shared';

@Component({
selector: 'app-espera',
templateUrl: './espera.component.html',
animations: [routerTransition()],
providers: [
    NgbModal,
    EsperaService]
})

export class EsperaComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Espera';

    selectedEntity: Espera;

    public constructor(public router: Router,
        private modalService: NgbModal,
        private esperaService: EsperaService) {
        this.selectedEntity = new Espera();
    }

    public ngOnInit(): void {
        this.esperaService.getByID(1)
            .then(
                response =>
                this.selectedEntity = response)
            .catch(e => this.handleError (e));
    }

    showMessage(content, message) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    public onSave(content) {
        this.esperaService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.showMessage(content, 'Se actualizo correctamente.');
                }
            }
        ).catch(e => this.handleError(e));
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        alert('Permio guardado correctamente');
        return Promise.reject(error.message || error);
    }
}
