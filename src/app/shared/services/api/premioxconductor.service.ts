import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

// import { Configuration } from './configuration';

/*
  IMPORTANTE: esta api se basa sobre la tabla premiosxcuentas
    pero se utiliza para premios por conductores
*/
export class PremioXConductor {
  public id: string;
  public idpremio: number;
  public premio: string;
  public idsponsor: string;
  public sponsor: string;
  public idcuenta: string;
  public cuenta: string;
  public fecpremio: string;
  public fecven: string;
  public idestado: number;
  public estado: string;
  public idmovil: string;
  public nromovil: string;
  public movil: string;
  public idconductor: string;
  public conductor: string;
  public idbitacora: string;
  public idestadoprexcond: number;
  public estadoprexcond: string;
  public feccanjeo: string;
  public feccanjeocond: string;
  public codigopremio: string;
  public direccion: string;
  public selected: number;
  constructor() {
    this.selected = 0;
  }
}

@Injectable()
export class PremioXConductorService {
  readonly GET_BYID = 'byid';
  readonly GET_RESUMENXESTADO = 'resumenxestado';
  readonly GET_BYCONDUCTORXMOVIL = 'byconductorxmovil';
  readonly PUT_CANJEOCOND = 'canjeocond';

  private apiUrl = '';
  private headers;
  constructor (private http: Http) {// , private configuration: Configuration) {
    // this.apiUrl = configuration.ServerWithApiUrl + 'premioxconductor';
    this.apiUrl = environment.apiUrl + 'premioxconductor';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<PremioXConductor[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as PremioXConductor[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  /*
  SECCION PREMIOS X CONDUCTOR
  */
  getResumenXEstado(idestado: number): Promise<PremioXConductor[]> {
    const customUrl = `${this.apiUrl}/${this.GET_RESUMENXESTADO}/${idestado}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as PremioXConductor[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }

  getByID(id: string): Promise<PremioXConductor> {
    const customUrl = `${this.apiUrl}/${this.GET_BYID}/${id}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then((response: Response) => {
          const body = response.json();
          const entity = body[0] as PremioXConductor;
          return entity;
        }
      ).catch(e => this.handleError(e));
  }

  getByIDConductorIDMovil(id: number): Promise<PremioXConductor[]>  {
    const customUrl = `${this.apiUrl}/${this.GET_BYCONDUCTORXMOVIL}/${id}`;

    return this.http
    .get(customUrl, { headers: this.headers })
    .toPromise()
    .then(response => {
      const body = response.json();
      const entities = body || {} as PremioXConductor[];
      return entities;
      }
    ).catch(e => this.handleError(e));
  }

  canjear(entity: PremioXConductor): Promise<PremioXConductor> {
    const url = `${this.apiUrl}/${this.PUT_CANJEOCOND}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      ).catch(e => this.handleError(e));
  }

  save(entity: PremioXConductor): Promise<PremioXConductor> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: PremioXConductor): Promise<PremioXConductor> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: PremioXConductor): Promise<PremioXConductor> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      ).catch(e => this.handleError(e));
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
