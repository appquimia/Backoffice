import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from '../../shared';

// relative import
import { TarifarioPlantillaRoutingModule } from './tarifarioplantilla-routing.module';
import { TarifarioPlantillaComponent } from './tarifarioplantilla.component';
import {
    TarifasComponent,
    TarifaComponent,
    FijasComponent,
    FijaComponent,
    EsperaComponent
} from './components';

@NgModule({
    imports: [
        CommonModule,
        TarifarioPlantillaRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule
    ],
    declarations: [
        TarifarioPlantillaComponent,
        TarifasComponent,
        TarifaComponent,
        FijasComponent,
        FijaComponent,
        EsperaComponent
    ]
})
export class TarifarioPlantillaModule { }
