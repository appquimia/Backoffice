import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PremiosXConductorRoutingModule } from './premiosxconductor-routing.module';
import { PremiosXConductorComponent } from './premiosxconductor.component';
import { PageHeaderModule } from '../../shared';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  ConductorComponent,
  PremiosComponent,
  ItemComponent
} from './components/';

@NgModule({
    imports: [
        CommonModule,
        PremiosXConductorRoutingModule,
        PageHeaderModule,
        FormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule,
        ReactiveFormsModule,
    ],
    declarations: [
        PremiosXConductorComponent,
        ConductorComponent,
        PremiosComponent,
        ItemComponent
    ]
})
export class PremiosXConductorModule { }
