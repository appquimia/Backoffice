import { Component, OnInit, Input, Output, EventEmitter, SimpleChange, ViewContainerRef, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    Contacto, ContactoService,
    TWUtils } from '../../../../shared/';

@Component({
    selector: 'app-contacto',
    templateUrl: './contacto.component.html',
    // styleUrls: ['./contacto.component.scss'],
    providers: [
        ContactoService,
    ]
})

export class ContactoComponent implements OnInit, OnChanges {
    @Input() selectedItem: Contacto;
    @Output() itemUpdated = new EventEmitter();

    public modalMessage: string;
    public modalTitle = 'Contacto';
    // variables de campos
    public selectedEntity: Contacto;
    private selectedIDSponsor: number;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private contactoService: ContactoService) {
            this.cancelar();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDSponsor = params['id'];
        });
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedItem'].currentValue !== null) {
            this.selectedItem = changes['selectedItem'].currentValue;
        }
    }

    onSubmit() {
        this.selectedItem.idsponsor = this.selectedIDSponsor;
        this.contactoService.save(this.selectedItem).then(
            response => {
                if (response) {
                    this.cancelar();
                    this.itemUpdated.emit(true);
                } else {
                    this.showMessage('No se han podido guardar los datos del contacto!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
      this.contactoService.delete(this.selectedItem)
      .then(
          response => {
            if (response) {
                this.itemUpdated.emit(true);
                this.cancelar();
            } else {
                this.showMessage('No se han podido eliminar los datos del contacto!!', false);
            }
        }).catch(e => this.handleError(e));
    }

    public cancelar() {
        this.selectedItem = new Contacto();
    }

    isNew() {
      return (this.selectedItem.id === -1);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/sponsors']);
                        } else {
                            this.router.navigate(['/sponsors', this.selectedIDSponsor]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
