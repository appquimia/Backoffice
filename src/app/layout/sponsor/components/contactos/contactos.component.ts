import { Component, OnInit, Input,  SimpleChange } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';

import { Contacto, ContactoService, Sponsor } from '../../../../shared/';

@Component({
    selector: 'app-contactos',
    templateUrl: './contactos.component.html',
    // styleUrls: ['./contactos.component.scss'],
    providers: [
        ContactoService]
})

export class ContactosComponent implements OnInit {
  @Input() private selectedEntity: Sponsor;
  settings = {
    noDataMessage: 'Datos no disponibles',
    pager: { display: true, perPage: 10 },
    columns: {
      apellidos: { title: 'Apellidos', filter: false, },
      nombres: { title: 'Nombre', filter: false, },
      cargo: { title: 'Cargo', filter: false, },
      email: { title: 'Correo', filter: false, },
      telefono: { title: 'Teléfono', filter: false, },
    },
    actions: { add: false, edit: false, delete: false, },
    attr: {
        class: 'table dataTable table-striped table-bordered table-hover',
    },
  };
  source: LocalDataSource;
  public data: Array<any> = Array<any>();
  public selectedItem: Contacto = null;
  public selectedIDSponsor: number;

  public constructor(public router: Router,
      private route: ActivatedRoute,
      private contactoService: ContactoService) {
        this.source = new LocalDataSource(this.data);
        this.selectedItem = new Contacto();
  }

  public ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.selectedIDSponsor = params['id'];
      this.getData();
    });
  }

  handleItemUpdated(user) {
    this.getData();
  }

  public getData() {
    if (this.selectedIDSponsor !== -1) {
      this.contactoService.getListBySponsor(this.selectedIDSponsor).then(
          response => {
            if (response) {
              this.data = response;
              this.source = new LocalDataSource(this.data);
            }else {
              alert('No se pudo obtener los datos de contactos!!');
            }
          }
        ).catch(e => this.handleError(e));
    }
  }

  onSearch(query: string) {
      if (query.length === 0) {
          this.source.setFilter([]);
          return;
      }
      this.source.setFilter([
          { field: 'apellidos', search: query, },
          { field: 'nombres', search: query, },
          { field: 'cargo', search: query, }
      ], false);
  }

  public userRowSelect(event: any): any {
      this.selectedItem = event.data;
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
