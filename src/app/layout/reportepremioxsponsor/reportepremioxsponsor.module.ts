import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ReportePremioXSponsorRoutingModule } from './reportepremioxsponsor-routing.module';
import { ReportePremioXSponsorComponent } from './reportepremioxsponsor.component';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        NgbModule.forRoot(),
        CommonModule,
        ReportePremioXSponsorRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [ReportePremioXSponsorComponent]
})
export class ReportePremioXSponsorModule { }
