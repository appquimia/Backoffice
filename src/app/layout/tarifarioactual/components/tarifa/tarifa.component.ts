import { Component, OnInit, Input, Output, EventEmitter,
    OnChanges, SimpleChange, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import {Tarifa, TarifaService} from '../../../../shared';

@Component({
    selector: 'app-tarifa',
    templateUrl: './tarifa.component.html',
    animations: [routerTransition()],
    providers: [
        NgbModal,
        TarifaService]
})

export class TarifaComponent implements OnInit, OnChanges, AfterViewChecked {
    @Input() selectedItem: Tarifa;
    @Output() itemUpdated = new EventEmitter();

    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Tarifa';

    selectedEntity: Tarifa;

    public constructor(public router: Router,
        private cdRef: ChangeDetectorRef,
        private modalService: NgbModal,
        private tarifaService: TarifaService) {
        this.selectedEntity = new Tarifa();
    }

    public ngAfterViewChecked() {
      this.cdRef.detectChanges();
    }

    public ngOnInit(): void {
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedItem'].currentValue !== null) {
            this.selectedEntity = changes['selectedItem'].currentValue;
        }
    }

    showMessage(content, message, action) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    public onSave(content) {
        this.tarifaService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedEntity = response;
                    this.cancelar();
                }
            }
        ).catch(e => this.handleError(e));
    }

    public cancelar() {
        this.selectedEntity = new Tarifa();
        this.itemUpdated.emit(true);
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        alert('Permio guardado correctamente');
        return Promise.reject(error.message || error);
    }
}
