import { Component, OnInit, Input,  SimpleChange } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';

import { Sucursal, SucursalService, Sponsor } from '../../../../shared/';

@Component({
    selector: 'app-sucursales',
    templateUrl: './sucursales.component.html',
    // styleUrls: ['./sucursales.component.scss'],
    providers: [
        SucursalService]
})

export class SucursalesComponent implements OnInit {
  @Input() private selectedEntity: Sponsor;
  settings = {
    noDataMessage: 'Datos no disponibles',
    pager: { display: true, perPage: 10 },
    columns: {
      sucursal: { title: 'Sucursal', filter: false, },
      direccion: { title: 'Dirección', filter: false, },
      descripcion: { title: 'Descripción', filter: false, }
    },
    actions: { add: false, edit: false, delete: false, },
    attr: {
        class: 'table dataTable table-striped table-bordered table-hover',
    },
  };
  source: LocalDataSource;
  public data: Array<any> = Array<any>();
  public selectedItem: Sucursal = null;
  public selectedIDSponsor: number;

  public constructor(public router: Router,
      private route: ActivatedRoute,
      private sucursalService: SucursalService) {
        this.source = new LocalDataSource(this.data);
        this.selectedItem = new Sucursal();
  }

  public ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.selectedIDSponsor = params['id'];
      this.getData();
    });
  }

  handleItemUpdated(user) {
    this.getData();
  }

  public getData() {
    if (this.selectedIDSponsor !== -1) {
      this.sucursalService.getListByIdSponsor(this.selectedIDSponsor).then(
          response => {
            if (response) {
              this.data = response;
              this.source = new LocalDataSource(this.data);
            }else {
              alert('No se pudo obtener los datos de sucursales!!');
            }
          }
        ).catch(e => this.handleError(e));
    }
  }

  onSearch(query: string) {
      if (query.length === 0) {
          this.source.setFilter([]);
          return;
      }
      this.source.setFilter([
          { field: 'sucursal', search: query, },
          { field: 'direccion', search: query, },
          { field: 'descripcion', search: query, }
      ], false);
  }

  public userRowSelect(event: any): any {
      this.selectedItem = event.data;
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
