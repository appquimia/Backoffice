import { Component, OnInit, Input, Output, EventEmitter, OnChanges,
        SimpleChange, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import {TarifaFijaPlantilla, TarifaPlantillaService} from '../../../../shared';

@Component({
    selector: 'app-fija',
    templateUrl: './fija.component.html',
    animations: [routerTransition()],
    providers: [
        NgbModal,
        TarifaFijaPlantilla]
})

export class FijaComponent implements OnInit, OnChanges, AfterViewChecked {
    @Input() selectedItem: TarifaFijaPlantilla;
    @Output() itemUpdated = new EventEmitter();

    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'TarifaFija';

    selectedEntity: TarifaFijaPlantilla;

    public constructor(public router: Router,
        private cdRef: ChangeDetectorRef,
        private modalService: NgbModal,
        private tarifaFijaService: TarifaPlantillaService) {
        this.selectedEntity = new TarifaFijaPlantilla();
    }

    public ngAfterViewChecked() {
      this.cdRef.detectChanges();
    }

    public ngOnInit(): void {
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedItem'].currentValue !== null) {
            this.selectedEntity = changes['selectedItem'].currentValue;
        }
    }

    showMessage(content, message, action) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    public onSave(content) {
        this.tarifaFijaService.saveFija(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedEntity = response;
                    this.cancelar();
                }
            }
        ).catch(e => this.handleError(e));
    }

    public cancelar() {
        this.selectedEntity = new TarifaFijaPlantilla();
        this.itemUpdated.emit(true);
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        alert('Permio guardado correctamente');
        return Promise.reject(error.message || error);
    }
}
