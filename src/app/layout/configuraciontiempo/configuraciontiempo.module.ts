import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ConfiguracionTiempoRoutingModule } from './configuraciontiempo-routing.module';
import { ConfiguracionTiempoComponent } from './configuraciontiempo.component';
import { PageHeaderModule } from './../../shared';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        ConfiguracionTiempoRoutingModule,
        PageHeaderModule,
        FormsModule,
        NgbModule.forRoot(),
        ReactiveFormsModule,
    ],
    declarations: [ConfiguracionTiempoComponent]
})
export class ConfiguracionTiempoModule { }
