import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportePremioXSponsorComponent } from './reportepremioxsponsor.component';

const routes: Routes = [
    { path: '', component: ReportePremioXSponsorComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ReportePremioXSponsorRoutingModule { }
