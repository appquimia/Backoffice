import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ng2-bootstrap';
import { Ng2TableModule } from 'ng2-table';

// relative import
import { ReclamoRoutingModule } from './reclamo-routing.module';
import { ReclamoComponent } from './reclamo.component';
import {
  ReclamoDataComponent,
  EventosComponent,
  CargaEventoComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        CommonModule,
        ReclamoRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2TableModule,
        PaginationModule.forRoot()
    ],
    declarations: [
      ReclamoComponent,
      ReclamoDataComponent,
      EventosComponent,
      CargaEventoComponent
    ]
})
export class ReclamoModule { }
