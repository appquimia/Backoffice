import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '', component: LayoutComponent,
        children: [
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'reclamos', loadChildren: './reclamos/reclamos.module#ReclamosModule' },
            { path: 'reclamo/:id', loadChildren: './reclamo/reclamo.module#ReclamoModule' },
            { path: 'sponsors', loadChildren: './sponsors/sponsors.module#SponsorsModule' },
            { path: 'sponsor/:id', loadChildren: './sponsor/sponsor.module#SponsorModule' },
            { path: 'calificaciones', loadChildren: './calificaciones/calificaciones.module#CalificacionesModule' },
            { path: 'movil/:idmovil/:idconductor', loadChildren: './movil/movil.module#MovilModule' },
            { path: 'premios', loadChildren: './premios/premios.module#PremiosModule' },
            { path: 'premio/:id', loadChildren: './premio/premio.module#PremioModule' },
            { path: 'publicidades', loadChildren: './publicidades/publicidades.module#PublicidadesModule' },
            { path: 'publicidad/:id', loadChildren: './publicidad/publicidad.module#PublicidadModule' },
            { path: 'premiosxconductores', loadChildren: './premiosxconductores/premiosxconductores.module#PremiosXConductoresModule' },
            { path: 'premiosxconductor/:id', loadChildren: './premiosxconductor/premiosxconductor.module#PremiosXConductorModule' },
            { path: 'reportepremiosxsponsor',
                loadChildren: './reportepremiosxsponsor/reportepremiosxsponsor.module#ReportePremiosXSponsorModule' },
            { path: 'reportepremioxsponsor/:idpremio',
                loadChildren: './reportepremioxsponsor/reportepremioxsponsor.module#ReportePremioXSponsorModule' },
            { path: 'tarifarioactual', loadChildren: './tarifarioactual/tarifarioactual.module#TarifarioActualModule' },
            { path: 'tarifarioplantilla', loadChildren: './tarifarioplantilla/tarifarioplantilla.module#TarifarioPlantillaModule' },
            { path: 'configuraciontiempo', loadChildren: './configuraciontiempo/configuraciontiempo.module#ConfiguracionTiempoModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
