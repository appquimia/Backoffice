import { Injectable } from '@angular/core';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ImagesBase64 } from '../images-base64.service'

import { Reporte, Premio } from '../../../shared'

import { DatePipe } from '@angular/common';

export class GenPremioDetalle {
    private lineHeight: number;
    private line: number;
    private leftMargin: number;
    private rightMargin: number;
    private dp: DatePipe;

    private mDoc;
    constructor() {
    }

    setTextCentered(text: string, start: number, end: number) {
        const init = (end - start) / 2 - text.length;
        this.mDoc.text(init, this.line * this.lineHeight, text);
        return init;
    }

    public genPdf(
        dp: DatePipe,
        premio: Premio,
        fecreporte: string,
        fecinicio: string,
        fecfin: string,
        lista: Reporte[]) {

        this.dp = dp;

        this.mDoc = new jsPDF().setProperties({
            title: 'Reporte: Detalle del premio',
            orientation: 'p',
            unit: 'cm',
            format: 'a4'
        });

        this.lineHeight = 3;
        this.line = 4;
        this.leftMargin = 8;
        this.rightMargin = 200;

        this.mDoc.addImage(ImagesBase64.logo, 'PNG', 8, 5, 63, 14);
        this.mDoc.setFont('cantarell').setFontType('normal').setFontSize(11).setTextColor(100);
        let outputText = 'Reporte: Detalle del premio';
        this.mDoc.text(this.rightMargin, this.line * this.lineHeight + 2, outputText, null, null, 'right');
        this.line++;
        outputText =  'Generado: ' + fecreporte;
        this.mDoc.text(this.rightMargin, this.line * this.lineHeight + 5, outputText, null, null, 'right');

        this.line++;
        this.line++;
        this.mDoc.line(this.leftMargin, this.line * this.lineHeight + 1, this.rightMargin, this.line * this.lineHeight + 1);

        this.line++;
        this.mDoc.setFont('helvetica').setFontType('bold').setFontSize(9).setTextColor(0);
        this.mDoc.text(this.leftMargin, this.line * this.lineHeight + 2, 'Sponsor: ');
        outputText = premio.sponsor;
        this.mDoc.setFont('helvetica').setFontType('normal').setFontSize(9).setTextColor(100);
        this.mDoc.text(this.leftMargin + 16, this.line * this.lineHeight + 2, outputText);

        this.line++;
        this.mDoc.setFont('helvetica').setFontType('bold').setFontSize(9).setTextColor(0);
        this.mDoc.text(this.leftMargin, this.line * this.lineHeight + 2, 'Premio: ');
        outputText = premio.codigo + ' - ' + premio.descripcion;
        this.mDoc.setFont('helvetica').setFontType('normal').setFontSize(9).setTextColor(100);
        this.mDoc.text(this.leftMargin + 16, this.line * this.lineHeight + 2, outputText);

        this.line++;
        this.mDoc.setFont('helvetica').setFontType('bold').setFontSize(9).setTextColor(0);
        this.mDoc.text(this.leftMargin, this.line * this.lineHeight + 2, 'Desde: ');
        outputText = fecinicio;
        this.mDoc.setFont('helvetica').setFontType('normal').setFontSize(8).setTextColor(100);
        this.mDoc.text(this.leftMargin + 16, this.line * this.lineHeight + 2, outputText);

        this.line++;
        this.mDoc.setFont('helvetica').setFontType('bold').setFontSize(9).setTextColor(0);
        this.mDoc.text(this.leftMargin, this.line * this.lineHeight + 2, 'Hasta: ');
        outputText = fecfin;
        this.mDoc.setFont('helvetica').setFontType('normal').setFontSize(8).setTextColor(100);
        this.mDoc.text(this.leftMargin + 16, this.line * this.lineHeight + 2, outputText);

        this.line++;
        this.line++;

        this.genAutoTableData(lista, this.line * this.lineHeight);

        this.mDoc.save('DetallePremio-' + premio.codigo + '.pdf');
    }

    protected genAutoTableData(items: Reporte[], top: number): number {
        const columns = ['Fecha', 'Conductor', 'Móvil Nº', 'Canjeado', 'Canjeado x conductor'];
        const rows = [];

        items.forEach(item => {
            const fecpremio = (item.fecpremio !== '0000-00-00') ? this.dp.transform(item.fecpremio, 'dd-MM-yyyy') : '';
            const feccanjeo = (item.feccanjeo !== '0000-00-00') ? this.dp.transform(item.feccanjeo, 'dd-MM-yyyy') : '';
            const feccanjeocond = (item.feccanjeocond !== '0000-00-00') ? this.dp.transform(item.feccanjeocond, 'dd-MM-yyyy') : '';
            rows.push([
                fecpremio,
                item.conductor,
                item.nromovil,
                feccanjeo,
                feccanjeocond]);
        });

        const style = {
                theme: 'plain',
                // styles: {fillColor: [0, 0, 0]},
                margin: {top: 150},
                // columnStyles: {
                //     Código: {fillColor: 255}
                // }
            };
        const colStyles = {
            2: {halign: 'right'},
            3: {halign: 'right'},
            4: {halign: 'right'},
        }
        return this.mDoc.autoTable({columns: columns, body: rows, styles: style, startY : top, columnStyles: colStyles});
    }

}
