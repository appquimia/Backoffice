import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { PaginationModule } from 'ng2-bootstrap';
import { Ng2TableModule } from 'ng2-table';

import { ReclamosRoutingModule } from './reclamos-routing.module';
import { ReclamosComponent } from './reclamos.component';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        FormsModule,
        Ng2TableModule,
        PaginationModule.forRoot(),
        CommonModule,
        ReclamosRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [ReclamosComponent]
})
export class ReclamosModule { }
