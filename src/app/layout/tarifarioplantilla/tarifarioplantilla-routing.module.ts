import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TarifarioPlantillaComponent } from './tarifarioplantilla.component';

const routes: Routes = [
    { path: '', component: TarifarioPlantillaComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TarifarioPlantillaRoutingModule { }
