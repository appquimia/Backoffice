import { Component, OnInit, Input,  SimpleChange } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';

import { Producto, ProductoService, Sponsor } from '../../../../shared/';

@Component({
    selector: 'app-productos',
    templateUrl: './productos.component.html',
    // styleUrls: ['./productos.component.scss'],
    providers: [
        ProductoService]
})

export class ProductosComponent implements OnInit {
  @Input() private selectedEntity: Sponsor;
  settings = {
    noDataMessage: 'Datos no disponibles',
    pager: { display: true, perPage: 10 },
    columns: {
      producto: { title: 'Producto', filter: false, },
      descripcion: { title: 'Descripción', filter: false, },
      imagen: {
                title: 'Imagen', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<img src="' + value + '" alt="Smiley face" height="75" width="75">';
                }
            }
    },
    actions: { add: false, edit: false, delete: false, },
    attr: {
        class: 'table dataTable table-striped table-bordered table-hover',
    },
  };
  source: LocalDataSource;
  public data: Array<any> = Array<any>();
  public selectedItem: Producto = null;
  public selectedIDSponsor: number;

  public constructor(public router: Router,
      private route: ActivatedRoute,
      private productoService: ProductoService) {
        this.source = new LocalDataSource(this.data);
        this.selectedItem = new Producto();
  }

  public ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.selectedIDSponsor = params['id'];
      this.getData();
    });
  }

  handleItemUpdated(user) {
    this.getData();
  }

  public getData() {
    if (this.selectedIDSponsor !== -1) {
      this.productoService.getListByIdSponsor(this.selectedIDSponsor).then(
          response => {
            if (response) {
              this.data = response;
              this.source = new LocalDataSource(this.data);
            }else {
              alert('No se pudo obtener los datos de contactos!!');
            }
          }
        ).catch(e => this.handleError(e));
    }
  }

  onSearch(query: string) {
      if (query.length === 0) {
          this.source.setFilter([]);
          return;
      }
      this.source.setFilter([
          { field: 'producto', search: query, },
          { field: 'descripcion', search: query, }
      ], false);
  }

  public userRowSelect(event: any): any {
      this.selectedItem = event.data;
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
