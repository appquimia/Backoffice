import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';

@NgModule({
    imports: [
        CommonModule,
        LoginRoutingModule
    ],
    declarations: [LoginComponent]
})
export class LoginModule {
  constructor(private translate: TranslateService) {
      //const browserLang = localStorage.getItem('language') ? 'es';
      //this.translate.use('es');
  }
}
