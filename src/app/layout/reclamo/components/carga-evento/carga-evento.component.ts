import { Component, OnInit, Input, Output, OnChanges, EventEmitter, SimpleChange  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import {
    Evento, EventoService,
    ReclamoEstado, ReclamoEstadoService,
    Usuario, UsuarioService
} from '../../../../shared/services/';

@Component({
    selector: 'app-cargaevento',
    templateUrl: './carga-evento.component.html',
    styleUrls: ['./carga-evento.component.scss'],
    providers: [
        EventoService,
        ReclamoEstadoService,
        UsuarioService
    ]
})
export class CargaEventoComponent implements OnInit {
    @Input() selectedEvento: Evento;
    @Output() userUpdated = new EventEmitter();

    public error = false;
    public loading = false;
    submitted = false;

    // variables de campos
    public iduser;
    public loggedUser;

    // datoa estructurados
    public selectedEntity: Evento;
    private selectedIDReclamo: string;

    // arreglos
    public dataReclamoEstado: Array<any> = Array<any>();

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private eventoService: EventoService,
        private reclamoestadoService: ReclamoEstadoService,
        private usuarioService: UsuarioService) {
        const jsonData = JSON.parse(localStorage.getItem('userLog'));
        this.loggedUser = jsonData.usuario;

        this.selectedEntity = new Evento();
        this.selectedEntity.usuario = this.loggedUser;
        this.error = false;
        this.loading = false;
    }

    public ngOnInit(): void {
        this.loading = false;
        this.route.params.subscribe(params => {
            this.selectedIDReclamo = params['id'];
            this.getData();
        });
    }

    // tslint:disable-next-line:use-life-cycle-interface
    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedEvento'].currentValue !== null) {
            this.selectedEntity = changes['selectedEvento'].currentValue;
        }
    }

    public getData() {
        this.usuarioService.getUsuarioByNombre(this.loggedUser).then(
            response => {
                this.loading = false;
                const usuario = response;
                this.iduser = usuario.id;
            }
        ).catch(e => this.handleError(e));

        this.reclamoestadoService.getList().then(
            response => {
                this.loading = false;
                this.dataReclamoEstado = response;
            }
        ).catch(e => this.handleError(e));
    }

    onSubmit() {
        this.selectedEntity.idreclamo = this.selectedIDReclamo;
        this.selectedEntity.idusuario = this.iduser;

        this.eventoService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.loading = false;
                    // this.selectedEntity = response;
                    this.userUpdated.emit(true);
                    this.cancelar();
                } else {
                    this.error = true;
                    this.loading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
      this.eventoService.delete(this.selectedEntity).then(
          response => {
              if (response) {
                  this.loading = false;
                  this.userUpdated.emit(true);
                  this.cancelar();
              } else {
                  this.error = true;
                  this.loading = false;
              }
          }
      ).catch(e => this.handleError(e));
    }

    public cancelar() {
        this.selectedEntity = new Evento();
        this.selectedEntity.usuario = this.loggedUser;
    }

    isNew() {
      return (this.selectedEntity === undefined);
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
