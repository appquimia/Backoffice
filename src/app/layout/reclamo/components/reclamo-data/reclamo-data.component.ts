import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
    Reclamo, ReclamoService,
    ReclamoEstado, ReclamoEstadoService,
    Premio, PremioService,
    Sponsor, SponsorService,
    Contacto, ContactoService,
    Cuenta, CuentaService
} from '../../../../shared/services';

@Component({
    selector: 'app-reclamodata',
    templateUrl: './reclamo-data.component.html',
    styleUrls: ['./reclamo-data.component.scss'],
    providers: [
      ReclamoService,
      ReclamoEstadoService,
      PremioService,
      SponsorService,
      ContactoService,
      CuentaService
    ]
})
export class ReclamoDataComponent implements OnInit {
    public error = false;
    public loading = false;
    submitted = false;

    //public dataReclamoEstado: Array<any> = Array<any>();
    public selectedEntity: Reclamo;
    public selectedID: string;
    public selectedPremio: Premio;
    public selectedSponsor: Sponsor;
    public selectedCuenta: Cuenta;
    public dataContactos: Array<any> = Array<any>();

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private reclamoService: ReclamoService,
        private reclamoestadoService: ReclamoEstadoService,
        private premioService: PremioService,
        private sponsorService: SponsorService,
        private contactosService: ContactoService,
        private cuentaService: CuentaService) {
        this.selectedEntity = new Reclamo();
        this.selectedPremio = new Premio();
        this.selectedSponsor = new Sponsor();
        this.selectedCuenta = new Cuenta();
        this.selectedID = '-1';
        this.error = false;
        this.loading = false;
    }

    public ngOnInit(): void {
        this.loading = false;
        this.route.params.subscribe(
            params => {
            this.selectedID = params['id'];
            // this.getData();
            this.onSetData();
        });
    }

    // public getData() {
    //     this.loading = true;
    //     Promise.all([
    //         this.getReclamosEstados()
    //     ]).then(
    //         this.onSetData()
    //         );
    // }

    // public getReclamosEstados() {
    //     this.reclamoestadoService.getList().then(
    //         response => {
    //             if (response) {
    //                 this.dataReclamoEstado = response;
    //             } else {
    //                 this.error = true;
    //             }
    //         }
    //     ).catch(e => this.handleError(e));
    // }

    public getPremio() {
        this.premioService.getByID(this.selectedEntity.idpremio).then(
            response => {
                if (response) {
                    this.selectedPremio = response;
                } else {
                    this.error = true;
                }
            }
        ).catch(e => this.handleError(e));
    }

    public getSponsor() {
        this.sponsorService.getByID(this.selectedEntity.idsponsor).then(
            response => {
                if (response) {
                    this.selectedSponsor = response;
                } else {
                    this.error = true;
                }
            }
        ).catch(e => this.handleError(e));
    }

    public getContactos() {
        this.contactosService.getListBySponsor(this.selectedEntity.idsponsor).then(
            response => {
                if (response) {
                    this.dataContactos = response;
                } else {
                    this.error = true;
                }
            }
        ).catch(e => this.handleError(e));
    }

    public getCuenta() {
        this.cuentaService.getByID(this.selectedEntity.idcuenta).then(
            response => {
                if (response) {
                    this.selectedCuenta = response;
                } else {
                    this.error = true;
                }
            }
        ).catch(e => this.handleError(e));
    }

    public onSetData(): any {
        if (this.selectedID !== '-1') {
            this.reclamoService.getByID(this.selectedID).then(
                response => {
                    this.loading = false;
                    this.selectedEntity = response as Reclamo;
                    this.getPremio();
                    this.getSponsor();
                    this.getContactos();
                    // this.getReclamosEstados();
                    this.getCuenta();
                }
            ).catch(e => this.handleError(e));
        }
    }

    volver() {
        this.router.navigate(['/reclamos']);
        // this.reclamoService.save(this.selectedEntity).then(
        //     response => {
        //         if (response) {
        //             this.loading = false;
        //             this.selectedEntity = response;
        //             alert('Reclamo guardado correctamente');
        //             this.router.navigate(['/reclamos']);
        //         } else {
        //             this.error = true;
        //             this.loading = false;
        //         }
        //     }
        // ).catch(e => this.handleError(e));
    }

    public verPremio() {
        this.router.navigate(['/premio', this.selectedPremio.id]);
    }

    public verSponsor() {
        this.router.navigate(['/sponsor', this.selectedSponsor.id]);
    }

    // public eliminar() {
    //     console.log('eliminar');
    // }

    public isNew() {
        return (this.selectedID === '-1');
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
