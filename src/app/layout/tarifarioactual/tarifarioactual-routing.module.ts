import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TarifarioActualComponent } from './tarifarioactual.component';

const routes: Routes = [
    { path: '', component: TarifarioActualComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TarifarioActualRoutingModule { }
