import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

// import { Configuration } from './configuration';

export class Espera {
  public id: number;
  public costo: string;
  constructor() {
    this.costo = '';
  }
}

@Injectable()
export class EsperaService {

  private apiUrl = '';
  private headers;
  constructor (private http: Http) {// , private configuration: Configuration) {
    // this.apiUrl = configuration.ServerWithApiUrl + 'premio';
    this.apiUrl = environment.apiUrl + 'tiempoespera';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<Espera[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Espera[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<Espera> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Espera;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: Espera): Promise<Espera> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Espera): Promise<Espera> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Espera): Promise<Espera> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: Espera): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
          .delete(customUrl, { headers: this.headers })
          .toPromise()
          .then(response =>  {
              if (response.status === 204) {
                return response.ok;
              } else if (response.status === 202) {
                return -1;
              }
          })
          .catch(
            error =>
            this.handleError(error)
          );
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
