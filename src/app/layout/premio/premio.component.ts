import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { LOCALE_ID } from '@angular/core';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

import {
    NgbDateCustomParserFormatter,
    Premio, PremioService,
    Sponsor, SponsorService,
    Clasificacion, ClasificacionService,
    Paquete, PaqueteService,
    UploadFileService, TWUtils
} from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './premio.component.html',
    styleUrls: ['./premio.component.scss'],
    animations: [routerTransition()],
    providers: [
        PremioService,
        SponsorService,
        ClasificacionService,
        PaqueteService,
        UploadFileService,
        {provide: LOCALE_ID, useValue: 'es-AR'},
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}]
})

export class PremioComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Premio';

    public error = false;
    public loading = false;
    submitted = false;
    public selectedID: number;
    public selectedEntity: Premio;
    public dataSponsors: Array<any> = Array<any>();
    public dataClasificaciones: Array<any> = Array<any>();
    public dataPaquetes: Array<any> = Array<any>();
    public fecInicio: {};
    public fecFin: {};
    public fecVen: {};

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modalService: NgbModal,
        private premioService: PremioService,
        private sponsorService: SponsorService,
        private clasificacionService: ClasificacionService,
        private paqueteService: PaqueteService,
        private uploadService: UploadFileService) {
        this.selectedEntity = new Premio();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.loading = true;
            this.selectedID = +params['id'];

            this.getData();
        });
    }

    public getData() {
        const p0 = this.sponsorService.getList();
        const p1 = this.clasificacionService.getList();
        const p2 = this.paqueteService.getList();
        Promise.all([
            p0, p1, p2
        ]).then(([result0, result1, result2]) => {
            this.dataSponsors = result0;
            this.dataClasificaciones = result1;
            this.dataPaquetes = result2;
            this.setData();
        }).catch(e => this.handleError(e));
    }

    public getSponsors(): any {
        this.sponsorService.getList()
        .then(
            response => {
                this.loading = false;
                this.dataSponsors = response as Sponsor[];
            })
        .catch(e => this.handleError(e));
    }

    public getClasificaciones(): any {
        this.clasificacionService.getList()
        .then(
            response => {
                this.loading = false;
                this.dataClasificaciones = response as Clasificacion[];
            })
        .catch(e => this.handleError(e));
    }

    public getPaquetes(): any {
        this.paqueteService.getList()
        .then(
            response => {
                this.loading = false;
                this.dataPaquetes = response as Paquete[];
            })
        .catch(e => this.handleError(e));
    }

    public setData(): any {
      if (this.selectedID > -1) {
          this.premioService.getByID(this.selectedID)
          .then(
              response => {
                  this.loading = false;
                  this.selectedEntity = response as Premio;
                  this.fecInicio = TWUtils.stringDateToJson(this.selectedEntity.fecinicio);
                  this.fecFin = TWUtils.stringDateToJson(this.selectedEntity.fecfin);
                  this.fecVen = TWUtils.stringDateToJson(this.selectedEntity.fecven);
                  this.loading = false;
              })
          .catch(e => this.handleError(e));
      }
    }

    fileChange(event) {
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            this.uploadService.upload(file).then(
                response => {
                    this.selectedEntity.imagen = response.generatedName;
                }).catch(e => this.handleError(e));
        }
    }

    public onSave(content) {
        this.selectedEntity.fecinicio = TWUtils.arrayDateToString(this.fecInicio);
        this.selectedEntity.fecfin = TWUtils.arrayDateToString(this.fecFin);
        this.selectedEntity.fecven = TWUtils.arrayDateToString(this.fecVen);
        this.premioService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.loading = false;
                    this.selectedID = Number(response);
                    this.showMessage(content, 'El premio se guardó correctamente', this.MESSAGE_GOTO);
                    // this.router.navigate(['/premios']);
                } else {
                    this.error = true;
                    this.loading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar(content) {
        this.premioService.delete(this.selectedEntity).then(
            response => {
              if (response) {
                  this.selectedID = -1;
                  this.showMessage(content, 'El premio se eliminó correctamente', this.MESSAGE_GOTO);
              } else {
                  alert('No se ha podido eliminar los datos del premio!!');
              }
          }).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }

    showMessage(content, message, action) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
            if (action === this.MESSAGE_GOTO) {
                this.router.navigate(['/premios']);
            }
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        alert('Permio guardado correctamente');
        return Promise.reject(error.message || error);
    }
}
