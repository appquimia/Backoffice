import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

// import { Configuration } from './configuration';

export class Clasificacion {
  public id: number;
  public clasificacion: string;
  public probabilidad: number;
}

@Injectable()
export class ClasificacionService {
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {// , private configuration: Configuration) {
    // this.apiUrl = configuration.ServerWithApiUrl + 'clasificacion';
    this.apiUrl = environment.apiUrl + 'clasificacion';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<Clasificacion[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Clasificacion[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<Clasificacion> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Clasificacion;
          return entity;
        }
      ).catch(e => this.handleError(e));
  }

  save(entity: Clasificacion): Promise<Clasificacion> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Clasificacion): Promise<Clasificacion> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Clasificacion): Promise<Clasificacion> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      ).catch(e => this.handleError(e));
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
