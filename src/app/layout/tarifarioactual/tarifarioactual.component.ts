import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router } from '@angular/router';

@Component({
    selector: 'app-form',
    templateUrl: './tarifarioactual.component.html',
    styleUrls: ['./tarifarioactual.component.scss'],
    animations: [routerTransition()],
    providers: [ ]
})

export class TarifarioActualComponent implements OnInit {

    public constructor(public router: Router) {
    }

    public ngOnInit(): void {
    }
}
