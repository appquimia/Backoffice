import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';

import {
    PremioXConductor,
    PremioXConductorService,
    ConductorXMovilService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './premiosxconductores.component.html',
    styleUrls: ['./premiosxconductores.component.scss'],
    animations: [routerTransition()],
    providers: [
        PremioXConductorService,
        ConductorXMovilService
    ]
})

export class PremiosXConductoresComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            conductor: { title: 'Conductor', filter: false, },
            nromovil: { title: 'Movil', filter: false, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;

    public selectedEntity: PremioXConductor;
    public error = false;
    public loading = false;
    public data: Array<any> = Array<any>();

    public constructor(public router: Router, private route: ActivatedRoute,
        private conductorxmovilService: ConductorXMovilService) {
            this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.conductorxmovilService.getList().then(
            response => {
                if (response) {
                    this.loading = false;
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                } else {
                    this.error = true;
                    this.loading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'conductor', search: query },
            { field: 'nromovil', search: query }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/premiosxconductor', event.data.id]);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
