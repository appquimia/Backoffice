import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { PremiosLista, Premio, PremioService, ReporteService, TWUtils } from '../../shared';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import { GenPremioDetalle } from 'app/shared/services/vouchers/genpremiodetalle';

@Component({
    selector: 'app-tables',
    templateUrl: './reportepremioxsponsor.component.html',
    styleUrls: ['./reportepremioxsponsor.component.scss'],
    animations: [routerTransition()],
    providers: [
        DatePipe,
        PremioService,
        ReporteService,
        {provide: LOCALE_ID, useValue: 'es-AR'}
    ]
})

export class ReportePremioXSponsorComponent implements OnInit {
    public modalTitle = 'Premio por sponsor';
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            fecpremio: {
                title: 'Fecha', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return this.dp.transform(value, 'dd-MM-yyyy');
                } },
            conductor: { title: 'Conductor', filter: false, },
            nromovil: { title: 'Móvil Nº', filter: false, },
            nombre: { title: 'Usuario', filter: false, },
            email: { title: 'Correo', filter: false, },
            feccanjeo: {
                title: 'Canjeado', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    // return (+value === 1) ? 'SI' : 'NO';
                    return (value !== '0000-00-00') ? (this.dp.transform(value, 'dd-MM-yyyy')) : '';
                }
            },
            feccanjeocond: {
                title: 'Canjeado Conductor', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    // return (+value === 1) ? 'SI' : 'NO';
                    return (value !== '0000-00-00') ? (this.dp.transform(value, 'dd-MM-yyyy')) : '';
                }
            }
         },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;

    public selectedEntity: PremiosLista;
    public fecInicio;
    public fecFin;
    public selectedIDSponsor: number;
    public selectedIDPremio: number;
    selectedPremio: Premio;
    public error = false;
    public loading = false;
    public data: Array<any> = Array<any>();
    public dataSponsors: Array<any> = Array<any>();

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private dp: DatePipe,
        private premioService: PremioService,
        private reporteService: ReporteService) {
            this.selectedPremio = new Premio();
            this.selectedIDSponsor = -1;
            this.fecInicio = {};
            this.fecFin = {};
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.loading = true;
            this.selectedIDPremio = +params['idpremio'];
            this.fecInicio = localStorage.getItem('fecinicio');
            this.fecFin = localStorage.getItem('fecfin');
            this.getData();
        });
    }

    public getData() {
        const p0 = this.premioService.getByID(this.selectedIDPremio);
        Promise.all([
            p0
        ]).then(([result0]) => {
            this.selectedPremio = result0;
            this.getPremios();
        }).catch(e => this.handleError(e));
    }

    public getPremios() {
        if (this.fecInicio !== undefined && this.fecFin !== undefined) {
            this.reporteService.getPremioXFecha(this.selectedIDPremio, this.fecInicio, this.fecFin).then(
                response => {
                    if (response) {
                        this.loading = false;
                        this.data = response;
                        this.source = new LocalDataSource(this.data);
                    } else {
                        this.showMessage('No se han podido cargar datos de premio por sponsor!!', false, false);
                        this.loading = false;
                    }
                }
            ).catch(
                e => this.handleError(e));
        } else { console.log('Error')}
    }

    export() {
        const fechamin = this.dp.transform(this.fecInicio, 'dd-MM-yyyy');
        const fechamax = this.dp.transform(this.fecFin, 'dd-MM-yyyy');
        const fecreporte = this.dp.transform(Date.now(), 'dd-MM-yyyy');
        if (this.fecInicio !== '' && this.fecFin !== '' && this.selectedPremio !== undefined) {
            const voucher = new GenPremioDetalle();
            voucher.genPdf(this.dp, this.selectedPremio, fecreporte, fechamin, fechamax, this.data);
        }
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'conductor', search: query, },
            { field: 'nromovil', search: query, }
        ], false);
    }

    showMessage(message: string, goBack: boolean, notAuth: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (notAuth) {
                            this.router.navigate(['/dashboard']);
                        }
                        if (goBack) {
                            this.router.navigate(['/dashboard']);
                        } else {
                            this.router.navigate(['/dashboard']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
