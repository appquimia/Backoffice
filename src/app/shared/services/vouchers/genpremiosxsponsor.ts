import { Injectable } from '@angular/core';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ImagesBase64 } from '../images-base64.service'

import { Sponsor, PremiosLista, Reporte } from '../../../shared'

export class GenPremiosXConductor {
    private lineHeight: number;
    private line: number;
    private leftMargin: number;
    private rightMargin: number;

    private mDoc;
    constructor() {
    }

    setTextCentered(text: string, start: number, end: number) {
        const init = (end - start) / 2 - text.length;
        this.mDoc.text(init, this.line * this.lineHeight, text);
        return init;
    }

    public genPdf(
        sponsor: Sponsor,
        fecreporte: string,
        fecinicio: string,
        fecfin: string,
        lista: Reporte[]) {

        this.mDoc = new jsPDF().setProperties({
            title: 'Reporte: Premios por sponsor',
            orientation: 'p',
            unit: 'cm',
            format: 'a4'
        });

        this.lineHeight = 3;
        this.line = 4;
        this.leftMargin = 8;
        this.rightMargin = 200;

        this.mDoc.addImage(ImagesBase64.logo, 'PNG', 8, 5, 63, 14);
        this.mDoc.setFont('cantarell').setFontType('normal').setFontSize(11).setTextColor(100);
        let outputText = 'Reporte: Premios por Sponsor';
        this.mDoc.text(this.rightMargin, this.line * this.lineHeight + 2, outputText, null, null, 'right');
        this.line++;
        outputText =  'Generado: ' + fecreporte;
        this.mDoc.text(this.rightMargin, this.line * this.lineHeight + 5, outputText, null, null, 'right');

        this.line++;
        this.line++;
        this.mDoc.line(this.leftMargin, this.line * this.lineHeight + 1, this.rightMargin, this.line * this.lineHeight + 1);

        this.line++;
        this.mDoc.setFont('helvetica').setFontType('bold').setFontSize(9).setTextColor(0);
        this.mDoc.text(this.leftMargin, this.line * this.lineHeight + 2, 'Sponsor: ');
        outputText = sponsor.razonsocial;
        this.mDoc.setFont('helvetica').setFontType('normal').setFontSize(9).setTextColor(100);
        this.mDoc.text(this.leftMargin + 16, this.line * this.lineHeight + 2, outputText);

        this.line++;
        this.mDoc.setFont('helvetica').setFontType('bold').setFontSize(9).setTextColor(0);
        this.mDoc.text(this.leftMargin, this.line * this.lineHeight + 2, 'Desde: ');
        outputText = fecinicio;
        this.mDoc.setFont('helvetica').setFontType('normal').setFontSize(9).setTextColor(100);
        this.mDoc.text(this.leftMargin + 16, this.line * this.lineHeight + 2, outputText);

        this.line++;
        this.mDoc.setFont('helvetica').setFontType('bold').setFontSize(9).setTextColor(0);
        this.mDoc.text(this.leftMargin, this.line * this.lineHeight + 2, 'Hasta: ');
        outputText = fecfin;
        this.mDoc.setFont('helvetica').setFontType('normal').setFontSize(9).setTextColor(100);
        this.mDoc.text(this.leftMargin + 16, this.line * this.lineHeight + 2, outputText);


        this.line++;
        this.line++;

        this.genAutoTableData(lista, this.line * this.lineHeight);
        // this.line++;
        // this.mDoc.setDrawColor(0);
        // this.mDoc.roundedRect(position + 78, this.line * this.lineHeight - 4, 11, 6, 2, 2, 'D');
        // this.mDoc.roundedRect(position + 89, this.line * this.lineHeight - 4, 11, 6, 2, 2, 'D');
        // this.mDoc.roundedRect(position + 100, this.line * this.lineHeight - 4, 11, 6, 2, 2, 'D');
        // this.mDoc.setFont('helvetica').setFontType('bold').setFontSize(9).setTextColor(150);
        // this.mDoc.text(position + 82, this.line * this.lineHeight, fecha[2]);
        // this.mDoc.text(position + 93, this.line * this.lineHeight, fecha[1]);
        // this.mDoc.text(position + 102, this.line * this.lineHeight, fecha[0]);

        // this.mDoc.setFont('helvetica').setFontType('bolditalic').setFontSize(7).setTextColor(0);
        // this.mDoc.text(position + 17, this.line * this.lineHeight, 'NO VÁLIDO COMO FACTURA', null, null, 'center');
        // this.mDoc.setLineWidth(0.5);
        // this.mDoc.line(position, this.line * this.lineHeight + 3, this.rightMargin, this.line * this.lineHeight + 3);

        // this.line++;
        // this.line++;
        // this.line++;
        // let text = 'Salta 1111 - Teléfono 0379 15-471-1468';
        // this.mDoc.setFont('helvetica').setFontType('italic').setFontSize(7).setTextColor(0);
        // this.mDoc.text(this.leftMargin, this.line * this.lineHeight + 2, text);

        // this.mDoc.setFont('helvetica').setFontType('italic').setFontSize(7).setTextColor(0);
        // this.mDoc.text(position, this.line * this.lineHeight, 'CUIT 30-71190198-8');
        // this.mDoc.text(this.rightMargin, this.line * this.lineHeight, 'INICIO DE ACTIVIDAD 17/05/2011', null, null, 'right');

        // this.line++;
        // this.line++;

        // this.line++;
        // this.line++;
        // text = 'Deudas';
        // this.mDoc.setFont('helvetica').setFontType('bold').setFontSize(14).setTextColor(0);
        // this.mDoc.text(this.leftMargin, this.line * this.lineHeight + 2, text);
        // this.mDoc.setLineWidth(0.5);
        // this.mDoc.line(position - 80, this.line * this.lineHeight + 3, this.rightMargin, this.line * this.lineHeight + 3);
        // this.genAutoTableData(itemsdeuda);

        // let pos = this.mDoc.autoTableEndPosY() * 0.351459804; // pos en mm
        // pos *= this.lineHeight;
        // this.line += pos;
        // text = 'Forma de Pago';
        // this.mDoc.setFont('helvetica').setFontType('bold').setFontSize(14).setTextColor(0);
        // this.mDoc.text(this.leftMargin, pos, text);
        // this.mDoc.setLineWidth(0.8);
        // this.mDoc.line(position - 80, pos + 3, this.rightMargin, pos + 3);
        // this.genAutoTableFC(itemsFormaPago);

        // pos = this.mDoc.autoTableEndPosY()  * 0.351459804;
        // pos *= this.lineHeight;
        // text = 'Total Deuda: ' + this.cp.transform(comprobante.totaldeuda);
        // this.mDoc.text(position, pos + 5, text);
        // this.mDoc.setLineWidth(0.5);
        // this.mDoc.line(position + 30, pos + 7, this.rightMargin, pos + 7);
        // this.line++;

        // this.line++;
        // this.line++;
        // text = 'Total a Pagar: ' + this.cp.transform(comprobante.monto);
        // this.mDoc.text(position, pos + 15, text);
        // this.mDoc.setLineWidth(0.5);
        // this.mDoc.line(position + 30, pos + 17, this.rightMargin, pos + 17);
        // this.line++;

        this.mDoc.save('PremiosXSponsor-' + sponsor.id + '.pdf');
    }

    protected genAutoTableData(items: Reporte[], top: number): number {
        const columns = ['Código', 'Descripción', 'Cantidad', 'Canjeado', 'Canjeado x conductor'];
        const rows = [];

        items.forEach(item => {
            rows.push([
                item.codigopremio,
                item.premio,
                item.cantidad,
                item.canjeado,
                item.canjeadocond]);
        });

        const style = {
                theme: 'plain',
                // styles: {fillColor: [0, 0, 0]},
                margin: {top: 150},
                // columnStyles: {
                //     Código: {fillColor: 255}
                // }
            };
        const colStyles = {
            2: {halign: 'right'},
            3: {halign: 'right'},
            4: {halign: 'right'},
        }
        return this.mDoc.autoTable({columns: columns, body: rows, styles: style, startY : top, columnStyles: colStyles});
    }

    // protected genAutoTableFC(itemsFormaPago: GastoItem[]) {
    //     const columns = ['Destino', 'Forma de Pago', 'Cheque Nº', 'Importe'];
    //     const rows = [];

    //     itemsFormaPago.forEach(item => {
    //         rows.push([
    //             item.origendestino,
    //             item.formapago,
    //             item.numero,
    //             this.cp.transform(+item.monto)]);
    //     });

    //     this.mDoc.autoTable(columns, rows,
    //         {
    //             startY : this.mDoc.autoTableEndPosY() + 5,
    //             theme: 'plain',
    //             // styles: {fillColor: [0, 0, 0]},
    //             margin: {top: 64},
    //             // columnStyles: {
    //             //     Código: {fillColor: 255}
    //             // }
    //         });
    // }

}
