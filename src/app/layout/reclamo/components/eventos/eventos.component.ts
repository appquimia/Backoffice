import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Evento, EventoService } from '../../../../shared/services/';

@Component({
    selector: 'app-eventos',
    templateUrl: './eventos.component.html',
    styleUrls: ['./eventos.component.scss'],
    providers: [ EventoService]
})

export class EventosComponent implements OnInit {
  public rows: Array<any> = [];
  public columns: Array<any> = [
    {
      title: 'Estado', name: 'estado',
      filtering: {filterString: '', placeholder: 'Filtrar por estado'}
    },
    {title: 'Observación', name: 'descripcion'},
    {
      title: 'Responsable', name: 'usuario',
      filtering: {filterString: '', placeholder: 'Filtrar por usuario'}
    },
    { title: 'Fecha y Hora', name: 'fechayhora' },
  ];
  public page = 1;
  public itemsPerPage = 10;
  public maxSize = 5;
  public numPages = 1;
  public length = 0;
  public config: any = {
      paging: true,
      sorting: {columns: this.columns},
      filtering: {filterString: ''},
      className: ['table-striped', 'table-bordered']
    };

  public error = false;
  public loading = false;
  public data: Array<any> = Array<any>();
  public selectedEvento: Evento = null;
  public selectedIDReclamo: string;

  public constructor(public router: Router,
      private route: ActivatedRoute,
      private eventoService: EventoService) {
    this.length = this.data.length;
  }

  public ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.selectedIDReclamo = params['id'];
      this.getData();
    });
  }

  handleUserUpdated() {
    this.getData();
  }

  public getData() {
    if (this.selectedIDReclamo !== '-1') {
      this.eventoService.getListByReclamo(this.selectedIDReclamo).then(
          response => {
            if (response) {
              this.loading = false;
              this.data = response;
              this.length = this.data.length;
              this.onChangeTable(this.config);
            }else {
              this.error = true;
              this.loading = false;
              this.length = 0;
            }
          }
        ).catch(e => this.handleError(e));
    }
  }

  public onCellClick(data: any): any {
    this.selectedEvento = data.row as Evento;
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  public changePage(page: any, data: Array<any> = this.data): Array<any> {
    const start = (page.page - 1) * page.itemsPerPage;
    const end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    const columns = this.config.sorting.columns || [];
    let columnName = void 0;
    let sort = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].toLowerCase().match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item:any) =>
        item[config.filtering.columnName].toLowerCase().match(this.config.filtering.filterString));
    }

    const tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if (item[column.name].toString().toLowerCase().match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    const filteredData = this.changeFilter(this.data, this.config);
    const sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }
}
