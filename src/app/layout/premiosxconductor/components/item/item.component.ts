import { Component, OnInit, Input, Output, EventEmitter, SimpleChange  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LOCALE_ID } from '@angular/core';

import {
    PremioXConductor, PremioXConductorService,
    TWUtils
} from '../../../../shared/';

@Component({
    selector: 'app-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.scss'],
    providers: [
        PremioXConductorService,
        {provide: LOCALE_ID, useValue: 'es-AR'}
    ]
})

export class ItemComponent implements OnInit {
    @Input() selectedPremio: PremioXConductor;
    @Output() itemUpdated = new EventEmitter();

    // datoa estructurados
    private selectedIDConductor: string;

    // constantes
    protected textNoResults = 'No encontrado';

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private premioxconductorService: PremioXConductorService) {
            this.selectedPremio = new PremioXConductor();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDConductor = params['id'];
        });
    }

    // tslint:disable-next-line:use-life-cycle-interface
    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedPremio'].currentValue !== null) {
            this.selectedPremio = changes['selectedPremio'].currentValue;
        }
    }

    onSubmit() {
        this.premioxconductorService.save(this.selectedPremio).then(
            response => {
                if (response) {
                    this.selectedPremio = response;
                    this.itemUpdated.emit(true);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public canjear() {
      this.premioxconductorService.canjear(this.selectedPremio)
      .then(
          response => {
            if (response) {
                this.itemUpdated.emit(true);
                this.selectedPremio = new PremioXConductor();
            }
        }).catch(e => this.handleError(e));
    }

    public isSeted() {
        return (this.selectedPremio.id === '');
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
