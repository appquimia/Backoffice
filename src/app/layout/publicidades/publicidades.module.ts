import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { PaginationModule } from 'ng2-bootstrap';
import { Ng2TableModule } from 'ng2-table';

import { PublicidadesRoutingModule } from './publicidades-routing.module';
import { PublicidadesComponent } from './publicidades.component';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        FormsModule,
        Ng2TableModule,
        PaginationModule.forRoot(),
        CommonModule,
        PublicidadesRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [PublicidadesComponent]
})
export class PublicidadesModule { }
