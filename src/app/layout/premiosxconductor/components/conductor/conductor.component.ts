import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import {ConductorXMovil, ConductorXMovilService} from '../../../../shared';

@Component({
    selector: 'app-data',
    templateUrl: './conductor.component.html',
    styleUrls: ['./conductor.component.scss'],
    animations: [routerTransition()],
    providers: [ConductorXMovilService]
})

export class ConductorComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Premio del conductor';

    public error = false;
    public loading = false;
    submitted = false;
    public selectedID: number;
    public selectedEntity: ConductorXMovil;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modalService: NgbModal,
        private conductorXMovilService: ConductorXMovilService) {
        this.selectedEntity = new ConductorXMovil();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.loading = true;
            this.selectedID = +params['id'];
            this.setData();
        });
    }

    public setData(): any {
        this.conductorXMovilService.getByID(this.selectedID)
        .then(
            response => {
                this.loading = false;
                this.selectedEntity = response as ConductorXMovil;
                this.loading = false;
            })
        .catch(e => this.handleError(e));
    }

    showMessage(content, message, action) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
            if (action === this.MESSAGE_GOTO) {
                this.router.navigate(['/premiosxconductores']);
            }
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        alert('Permio guardado correctamente');
        return Promise.reject(error.message || error);
    }
}
