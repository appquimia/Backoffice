import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';

import { Sponsor, SponsorService } from '../../shared/services';

@Component({
    selector: 'app-tables',
    templateUrl: './sponsors.component.html',
    styleUrls: ['./sponsors.component.scss'],
    animations: [routerTransition()],
    providers: [
        SponsorService
    ]
})

export class SponsorsComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
          id: { title: 'Código', filter: false, },
          razonsocial: { title: 'Razón Social', filter: false, },
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;

    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: Sponsor;
    public selectedEstado: number;
    public error = false;
    public loading = false;
    public data: Array<any> = Array<any>();

    public constructor(public router: Router,
        private sponsorService: SponsorService) {
        this.selectedEstado = 1;
        this.data = [];
        this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.sponsorService.getList().then(
            response => {
                if (response) {
                    this.loading = false;
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                } else {
                    this.error = true;
                    this.loading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    nuevo() {
        this.router.navigate(['/sponsor', -1]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            {
                field: 'id',
                search: query,
            },
            {
                field: 'razonsocial',
                search: query,
            }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/sponsor', event.data.id]);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
