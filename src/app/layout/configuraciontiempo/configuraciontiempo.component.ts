import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import {
    ConfiguracionTiempo, ConfiguracionTiempoService
} from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './configuraciontiempo.component.html',
    // styleUrls: ['./configuraciontiempo.component.scss'],
    animations: [routerTransition()],
    providers: [
        ConfiguracionTiempoService]
})

export class ConfiguracionTiempoComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Configuracion de tiempo';

    public error = false;
    public loading = false;
    submitted = false;
    public selectedID: number;
    public selectedEntity: ConfiguracionTiempo;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modalService: NgbModal,
        private configuracionTiempoService: ConfiguracionTiempoService) {
        this.selectedEntity = new ConfiguracionTiempo();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.loading = true;
            this.getData();
        });
    }

    public getData(): any {
        this.configuracionTiempoService.getByID(1)
        .then(
            response => {
                this.loading = false;
                this.selectedEntity = response as ConfiguracionTiempo;
                this.loading = false;
            })
        .catch(e => this.handleError(e));
    }

    public guardar(content) {
        this.configuracionTiempoService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.loading = false;
                    this.selectedID = Number(response);
                    this.showMessage(content, 'La configuración se guardó correctamente', this.MESSAGE_GOTO);
                } else {
                    this.error = true;
                    this.loading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar(content) {
        this.configuracionTiempoService.delete(this.selectedEntity).then(
            response => {
              if (response) {
                  this.selectedID = -1;
                  this.showMessage(content, 'La configuración se eliminó correctamente', this.MESSAGE_GOTO);
              } else {
                  alert('No se ha podido eliminar los datos de la configuración!!');
              }
          }).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }

    showMessage(content, message, action) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
            // if (action === this.MESSAGE_GOTO) {
            //     this.router.navigate(['/dashboard']);
            // }
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        alert('Permio guardado correctamente');
        return Promise.reject(error.message || error);
    }
}
