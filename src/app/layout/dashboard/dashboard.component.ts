import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

import {
    Resumen, ReclamoService
} from '../../shared';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()],
    providers: [ReclamoService]
})
export class DashboardComponent implements OnInit {
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];

    public resumen: Resumen;

    constructor(
        private reclamoService: ReclamoService) {
            this.resumen = new Resumen();
        this.sliders.push({
            imagePath: 'assets/images/slider1.jpg',
            label: 'Grandes Premios',
            text: 'Jugá en tu celular, durante tu viaje con remises Apipe o en la Calle.'
        }, {
            imagePath: 'assets/images/slider2.jpg',
            label: 'Tarifas y Destinos Fijos',
            text: 'Verificá el costo de tus viajes urbanos o a destinos fijos.'
        }, {
            imagePath: 'assets/images/slider3.jpg',
            label: 'Se Auspiciante',
            text: 'Consúltanos para convertirte en sponsor.'
        });
    }

    ngOnInit() {
        this.getData();
    }

    public getData(): any {
        this.reclamoService.getResumen().then(
            response => {
                this.resumen = response as Resumen;
            }
        ).catch(e => this.handleError(e));
    }

    // public reclamosTotal() {
    //     console.log('reclamosTotal');
    // }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
