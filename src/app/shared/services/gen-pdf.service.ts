import { Injectable } from '@angular/core';
import * as jsPDF from 'jspdf';

import { PremioXConductor } from './api/premioxconductor.service';
import { ImagesBase64 } from './images-base64.service';
import { DatePipe } from '@angular/common';
import { TWUtils } from './tw-utils';
import { promise } from 'protractor';

@Injectable()
export class GenPdfService {
    private renglones: number;
    private lineHeight: number;
    private line: number;
    private leftMargin: number;
    private rightMargin: number;

    private mDoc;
    constructor(private datePipe: DatePipe) {
    }
    genFormHeader() {
        const doc = new jsPDF().setProperties({
            title: 'Ticket', orientation: 'p',
            unit: 'cm',
            format: 'a4'
        });

        this.lineHeight = 3;
        this.line = 8;
        this.leftMargin = 8;
        this.rightMargin = 200;

        doc.addImage(ImagesBase64.icono, 'PNG', 8, 5, 40, 14);
        doc.setFont('Cantarell').setFontType('normal').setFontSize(7).setTextColor(150);
        doc.text(this.leftMargin, this.line * this.lineHeight, 'AV. MAIPU 3256 - (3400) CORRIENTES');
        this.line++;
        doc.text(this.leftMargin, this.line * this.lineHeight, 'TEL. FAX: (0379) 4476396');
        doc.text(this.rightMargin, this.line * this.lineHeight, 'e-mail: info@azife.com', null, null, 'right');
        doc.line(this.leftMargin, this.line * this.lineHeight + 1, this.rightMargin, this.line * this.lineHeight + 1);
        doc.line(this.leftMargin, this.line * this.lineHeight + 2, this.rightMargin, this.line * this.lineHeight + 2);
        return doc;
    }

    public genCanjeo(data: PremioXConductor[]): Promise<boolean> {
        const doc = new jsPDF().setProperties({
            title: 'Cuponera', orientation: 'p',
            unit: 'cm',
            format: 'a4'
        });
        return new Promise(resolve => {
            this.lineHeight = 2.5;
            this.line = 4;
            this.leftMargin = 8;
            this.rightMargin = 204;
            this.renglones = 10;
            const paginas = Math.ceil(data.length / this.renglones);

            let renglon = 0;
            this.dottedLine(doc,
                this.leftMargin, this.line * this.lineHeight + 2.5,
                this.rightMargin, this.line * this.lineHeight + 2.5, 1);
            // doc.line(this.leftMargin, this.line * this.lineHeight, this.rightMargin, this.line * this.lineHeight);
            this.line++;
            this.line++;
            this.line++;
            data.forEach(premio => {
                doc.setFont('arial').setFontType('normal').setFontSize(8).setTextColor(0);
                doc.text(this.leftMargin, this.line * this.lineHeight, premio.conductor );
                doc.addImage(ImagesBase64.icono, 'PNG', this.leftMargin + 12, this.line * this.lineHeight, 40, 14);
                doc.addImage(ImagesBase64.icono, 'PNG', this.leftMargin + 72, this.line * this.lineHeight, 40, 14);
                doc.addImage(ImagesBase64.icono, 'PNG', this.leftMargin + 140, this.line * this.lineHeight, 40, 14);
                let value = TWUtils.padLeft(premio.nromovil, '0', 4);

                // Campo nro móvil
                doc.setFontStyle('bold');
                doc.text(this.rightMargin - 6, this.line * this.lineHeight, 'Movil Nº ', null, null, 'right');
                doc.setFontStyle('normal');
                doc.text(this.rightMargin, this.line * this.lineHeight, value, null, null, 'right');
                this.line++;
                this.line++;

                // Campo sponsor
                doc.setFontStyle('bold');
                doc.text(this.leftMargin, this.line * this.lineHeight, premio.sponsor);

                // Campo premio
                value = ' (' + premio.codigopremio + ') ' + premio.premio;
                doc.setFontStyle('normal');
                doc.text(this.rightMargin, this.line * this.lineHeight, value, null, null, 'right');
                let textWidth = (doc.getStringUnitWidth(value) * 8.2) / 3;
                doc.setFontStyle('bold');
                doc.text(this.rightMargin - (textWidth + 3), this.line * this.lineHeight, 'Premio: ', null, null, 'right');
                this.line++;
                this.line++;

                // Campo fecha
                value = 'Fecha: ';
                doc.setFontStyle('bold');
                doc.text(this.leftMargin, this.line * this.lineHeight, 'Fecha: ');
                textWidth = (doc.getStringUnitWidth(value) * 8) / 3;
                doc.setFontStyle('normal');
                doc.text(this.leftMargin + (textWidth + 2), this.line * this.lineHeight, this.datePipe.transform(premio.fecpremio));

                // Campo vencimiento
                value = this.datePipe.transform(premio.fecven);
                doc.setFontStyle('normal');
                doc.text(this.rightMargin, this.line * this.lineHeight, value, null, null, 'right');
                textWidth = (doc.getStringUnitWidth(value) * 8) / 3;
                doc.setFontStyle('bold');
                doc.text(this.rightMargin - (textWidth + 2), this.line * this.lineHeight, 'Vencimiento: ', null, null, 'right');

                this.line++;
                this.line++;
                // Campo direccion
                value = 'Dirección: ';
                doc.setFontStyle('bold');
                doc.text(this.leftMargin, this.line * this.lineHeight, 'Dirección: ');
                textWidth = (doc.getStringUnitWidth(value) * 8) / 3;
                doc.setFontStyle('normal');
                doc.text(this.leftMargin + (textWidth + 2), this.line * this.lineHeight, premio.direccion);
                this.dottedLine(doc,
                    this.leftMargin, this.line * this.lineHeight + 2.5,
                    this.rightMargin, this.line * this.lineHeight + 2.5, 1);
                renglon++;
                if (renglon > this.renglones) {
                    doc.addPage();
                    renglon = 0;
                    this.line = 4;
                    this.dottedLine(doc,
                        this.leftMargin, this.line * this.lineHeight + 2.5,
                        this.rightMargin, this.line * this.lineHeight + 2.5, 1);
                }
                this.line++;
                this.line++;
                this.line++;
            });
            doc.save('Cuponera.pdf');
            resolve(true);
        });
    }

    stringDate(strindDate: any) {
        const datestring = strindDate.split('-');
        const newDate = new Date(+datestring[0], (+datestring[1]) - 1, +datestring[2], 0, 0, 0, 0);
        const months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
        'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        if (newDate.toString() !== 'Invalid Date' && strindDate !== '0000-00-00') {
            return 'Corrientes ' + newDate.getDate() + ' de ' + months[newDate.getMonth()] + ' de ' + newDate.getFullYear();
        } else {
            return ''
        };
    }

    testHoja() {
        const doc = new jsPDF().setProperties({
            title: 'Presupuesto', orientation: 'p',
            unit: 'cm',
            format: 'a4'
        });
        this.line = 5;
        this.leftMargin = 8;
        this.lineHeight = 3;
        let text = '1';

        doc.setFont('Cantarell').setFontType('normal').setFontSize(8).setTextColor(150);
        do {
            this.line++;
            text = (this.line - 5) + '';
            doc.text(this.leftMargin, this.line * this.lineHeight, text);
        } while (this.line < 90);
    }

    dottedLine(doc, xFrom, yFrom, xTo, yTo, segmentLength) {
        // Calculate line length (c)
        const a = Math.abs(xTo - xFrom);
        const b = Math.abs(yTo - yFrom);
        const c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));

        // Make sure we have an odd number of line segments (drawn or blank)
        // to fit it nicely
        const fractions = c / segmentLength;
        const adjustedSegmentLength = (Math.floor(fractions) % 2 === 0) ? (c / Math.ceil(fractions)) : (c / Math.floor(fractions));

        // Calculate x, y deltas per segment
        const deltaX = adjustedSegmentLength * (a / c);
        const deltaY = adjustedSegmentLength * (b / c);

        let curX = xFrom, curY = yFrom;
        while (curX <= xTo && curY <= yTo) {
            doc.line(curX, curY, curX + deltaX, curY + deltaY);
            curX += 2 * deltaX;
            curY += 2 * deltaY;
        }
    }
}
