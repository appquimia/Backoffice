import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common';
import { GenPdfService } from '../../../../shared';

import {
    PremioXConductor,
    PremioXConductorService,
    PremioEstadoService} from '../../../../shared/';

@Component({
    selector: 'app-items',
    templateUrl: './premios.component.html',
    styleUrls: ['./premios.component.scss'],
    providers: [
        DatePipe,
        GenPdfService,
        PremioXConductorService,
        PremioEstadoService,
    ]
})

export class PremiosComponent implements OnInit {
    settings = {
        selectMode: 'multi',
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 100},
        columns: {
            premio: { title: 'Premio', filter: false, },
            sponsor: { title: 'Sponsor', filter: false, },
            estadoprexcond: { title: 'Estado', filter: false, },
            fecpremio: {
                title: 'Fecha', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            },
            fecven: {
                title: 'Vencimiento', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered table-responsive',
        },
    };
    source: LocalDataSource;

    public dataEstados: Array<any> = Array<any>();
    public selectedEstado: number;
    public selectedIdConductorXMovil: number;
    public selectedPremio: PremioXConductor;
    public error = false;
    public loading = false;
    public data: Array<any> = Array<any>();
    public selecteds: Array<any> = Array<any>();

    public constructor(public router: Router, private route: ActivatedRoute,
        private pdf: GenPdfService,
        private datePipe: DatePipe,
        private premioXConductorService: PremioXConductorService,
        private premioEstadoService: PremioEstadoService) {
        this.selectedEstado = 1;
        this.selectedPremio = new PremioXConductor();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.loading = true;
            this.premioEstadoService.getList().then(
                response => {
                    if (response) {
                        this.loading = false;
                        this.dataEstados = response;
                    } else {
                        this.error = true;
                        this.loading = false;
                    }
                }
            )
            this.selectedIdConductorXMovil = +params['id'];
            this.getData();
        });
    }

    getData() {
        this.premioXConductorService.getByIDConductorIDMovil(this.selectedIdConductorXMovil).then(
            response => {
                if (response) {
                    this.loading = false;
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                    this.filterByEstado();
                } else {
                    this.error = true;
                    this.loading = false;
                }
            }
        ).catch(this.handleError);
    }

    public filterByEstado() {
        const temp = this.data.filter(data => {
            return (+data.idestadoprexcond === +this.selectedEstado);
        });
        this.source = new LocalDataSource(temp);
    }

    public imprimir() {
        // this.selecteds = this.data.filter(data => {
        //     return data.selected === 1;
        // });
        this.pdf.genCanjeo(this.selecteds)
            .then(value => {
                if (value) {
                    this.selecteds.forEach(data => {
                        this.setCanjeados(data);
                    });
                }
            });
        this.getData();
    }

    setCanjeados(premio: PremioXConductor) {
        this.premioXConductorService.canjear(premio).then(
            response => {
                if (response) {
                    this.error = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'conductor', search: query, },
            { field: 'premio', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        // this.selectedPremio = event.data;
        // const premio = this.data.find(data => {
        //     return data.id === event.data.id
        // });
        // const index = this.data.indexOf(premio);
        // premio.selected = (premio.selected === 1) ? 0 : 1;
        // this.data[index] = premio;

        this.selecteds = event.selected;
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
