import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PremiosXConductorComponent } from './premiosxconductor.component';

const routes: Routes = [
    { path: '', component: PremiosXConductorComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PremiosXConductorRoutingModule { }
