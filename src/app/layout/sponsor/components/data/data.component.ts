import { Component, OnInit, ViewContainerRef, OnChanges, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import {
    Sponsor, SponsorService,
    UploadFileService } from '../../../../shared';

@Component({
    selector: 'app-data',
    templateUrl: './data.component.html',
    // styleUrls: ['./data.component.scss'],
    providers: [
        Sponsor,
        SponsorService,
        UploadFileService]
})

export class DataComponent implements OnInit, OnChanges {
    @Input() selectedEntity: Sponsor;

    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Sponsor';
    public error = false;
    public loading = false;
    submitted = false;

    public dataClientes: Array<any> = Array<any>();
    public dataFormasCobros: Array<any> = Array<any>();

    public selectedID: number;
    public estado;

    isLoading = false;

    public constructor(
        public router: Router,
        private route: ActivatedRoute,
        private modalService: NgbModal,
        private sponsorService: SponsorService,
        private uploadService: UploadFileService) {
            this.isLoading = true;
            const jsonData = JSON.parse(localStorage.getItem('userLog'));
            this.selectedEntity = new Sponsor();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getData();
        });
    }

    public getData() {
        if (this.selectedID > -1) {
            this.sponsorService.getByID(this.selectedID)
            .then(
                response => {
                    this.selectedEntity = response as Sponsor;
                    this.isLoading = false;
                })
            .catch(e => this.handleError(e));
        } else {
            this.isLoading = false;
        }
    }

    ngOnChanges() {
    }

    fileChange(event) {
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            this.uploadService.upload(file).then(
                response => {
                    this.selectedEntity.imagen = response.generatedName;
                }).catch(e => this.handleError(e));
        }
    }

    public guardar(content) {
        this.sponsorService
            .save(this.selectedEntity)
            .then(
                response => {
                    if (response > -1) {
                        this.loading = false;
                        this.selectedID = Number(response);
                        this.showMessage(content, 'El sponsor se guardó correctamente', this.MESSAGE_GOTO);
                    } else {
                        this.showMessage(content, 'Ya existe un sponsor con la razón social indicada!', this.MESSAGE_STAYHERE);
                        this.error = true;
                        this.loading = false;
                    }
                })
            .catch(e => this.handleError(e));
    }

    public eliminar(content) {
        this.sponsorService.delete(this.selectedEntity).then(
            response => {
              if (response) {
                  this.selectedID = -1;
                  this.showMessage(content, 'El sponsor se eliminó correctamente', this.MESSAGE_GOTO);
              } else {
                  alert('No se ha podido eliminar los datos del sponsor!!');
              }
          }).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }

    showMessage(content, message, action) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
            if (action === this.MESSAGE_GOTO) {
                this.router.navigate(['/sponsors']);
            }
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
