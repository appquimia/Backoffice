export const environment = {
  production: true,
  apiUrl: 'https://appquimia.com/adminoffice/api/',
  imagestock: 'https://appquimia.com/adminoffice/' + 'imagestock/',
  DEFAULT_PASS: 'AppquimiaPass'
};
