import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class ConfiguracionTiempo {
    public id: string;
    public moviles: number;
    public street: number;
    public commerce: number;

    constructor() {
        this.id = '';
    }
}

@Injectable()
export class ConfiguracionTiempoService {
    private apiUrl = '';
    private headers;
    constructor(private http: Http) {
        this.apiUrl = environment.apiUrl + 'configuraciontiempo';
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Cache-Control', 'no-cache');
        this.headers.append('Pragma', 'no-cache');
    }

    getList(): Promise<ConfiguracionTiempo[]> {
        const customUrl = `${this.apiUrl}`;

        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as ConfiguracionTiempo[];
                return entities;
            }
            )
            .catch(e => this.handleError(e));
    }

    getByID(id: number): Promise<ConfiguracionTiempo> {
        return this.http
            .get(this.apiUrl + '/' + id, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body[0] as ConfiguracionTiempo;
                return entity;
            }
            )
            .catch(e => this.handleError(e));
    }

    save(entity: ConfiguracionTiempo): Promise<ConfiguracionTiempo> {
        return this.put(entity);
    }

    private post(entity: ConfiguracionTiempo): Promise<ConfiguracionTiempo> {
        return this.http
            .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                return body.status === 'success';
            }
            )
            .catch(e => this.handleError(e));
    }

    private put(entity: ConfiguracionTiempo): Promise<ConfiguracionTiempo> {
        const url = `${this.apiUrl}/${entity.id}`;

        return this.http
            .put(url, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                return body.status === 'success';
            }
            )
            .catch(e => this.handleError(e));
    }

    delete(entity: ConfiguracionTiempo): Promise<Response> {
        const customUrl = `${this.apiUrl}/${entity.id}`;

        return this.http
            .delete(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                if (response.status === 204) {
                    return response.ok;
                } else if (response.status === 202) {
                    return -1;
                }
            })
            .catch(
                error =>
                    this.handleError(error)
            );
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
