import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PremiosXConductoresComponent } from './premiosxconductores.component';

const routes: Routes = [
    { path: '', component: PremiosXConductoresComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PremiosXConductoresRoutingModule { }
