import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PaginationModule } from 'ng2-bootstrap';
import { Ng2TableModule } from 'ng2-table';

import { CalificacionesRoutingModule } from './calificaciones-routing.module';
import { CalificacionesComponent } from './calificaciones.component';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        FormsModule,
        Ng2TableModule,
        PaginationModule.forRoot(),
        NgbModule.forRoot(),
        CommonModule,
        CalificacionesRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [CalificacionesComponent]
})
export class CalificacionesModule { }
