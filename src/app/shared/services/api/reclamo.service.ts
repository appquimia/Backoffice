import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

// import { Configuration } from './configuration';

export class Reclamo {
  public id: string;
  public idpremio: number;
  public idcuenta: string;
  public idestado: number;
  public estado: string;
  public fecha: string;
  public idsponsor: number;
  public razonsocial: string;
  public descripcion: string;
  public nombre: string;
}

export class Resumen {
  public total: number;
  public pendientes: number;
  public atendidos: number;
  public cerrados: number;
}

@Injectable()
export class ReclamoService {
  readonly GET_LISTBYESTADO = 'e';
  readonly GET_RESUMEN = 'r';

  private apiUrl = '';
  private headers;
  constructor (private http: Http) {// , private configuration: Configuration) {
    // this.apiUrl = configuration.ServerWithApiUrl + 'reclamo';
    this.apiUrl = environment.apiUrl + 'reclamo';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    this.headers.append('Pragma', 'no-cache');
  }

  getList(idestado: number): Promise<Reclamo[]> {
    const customUrl = `${this.apiUrl}/${this.GET_LISTBYESTADO}${idestado}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(
        response => {
          const body = response.json();
          const entities = body || {} as Reclamo[];
          return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: string): Promise<Reclamo> {
    const customUrl = encodeURI(`${this.apiUrl}`);
    const obj = { 'id': id};

    return this.http
      .patch(customUrl, JSON.stringify(obj), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Reclamo;
          return entity;
        }
      )
      .catch(this.handleError);
  }

  save(entity: Reclamo): Promise<Reclamo> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Reclamo): Promise<Reclamo> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(this.handleError);
  }

  private put(entity: Reclamo): Promise<Reclamo> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(this.handleError);
  }

  getResumen(): Promise<Resumen> {
    const customUrl = `${this.apiUrl}/${this.GET_RESUMEN}`;

    return this.http
    .get(customUrl, { headers: this.headers })
    .toPromise()
    .then(response => {
        const body = response.json();
        const entity = body[0] as Resumen;
        return entity;
      }
    )
    .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
