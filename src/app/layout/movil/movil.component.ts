import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Headers, Http, Response, RequestOptions } from '@angular/http';

import {
    CalificacionResumen,
    Calificacion,
    CalificacionService
} from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './movil.component.html',
    styleUrls: ['./movil.component.scss'],
    animations: [routerTransition()],
    providers: [CalificacionService]
})
export class MovilComponent implements OnInit {
    public error = false;
    public loading = false;
    submitted = false;
    public selectedIDMovil: string;
    public selectedIDConductor: string;
    public selectedEntity: CalificacionResumen;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private calificacionService: CalificacionService) {
        this.selectedEntity = new CalificacionResumen();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.loading = true;
            this.selectedIDMovil = params['idmovil'];
            this.selectedIDConductor = params['idconductor'];

            this.getData();
        });
    }

    public getData(): any {
      if (this.selectedIDMovil !== '' && this.selectedIDConductor !== '') {
          this.calificacionService.getByIDs(this.selectedIDMovil, this.selectedIDConductor)
          .then(
              response => {
                  this.loading = false;
                  this.selectedEntity = response as CalificacionResumen;
                  this.loading = false;
              })
          .catch(e => this.handleError(e));
      }
    }

    onSubmit() {
        this.router.navigate(['/calificaciones']);
    }

    updateUrl() {
        this.selectedEntity.imagen = 'https://placehold.it/400x400';
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
