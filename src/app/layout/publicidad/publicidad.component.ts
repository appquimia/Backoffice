import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { Headers, Http, Response, RequestOptions } from '@angular/http';

import {
    Publicidad, PublicidadService,
    UploadFileResponse, UploadFileService,
    Clasificacion, ClasificacionService,
    TWUtils
} from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './publicidad.component.html',
    styleUrls: ['./publicidad.component.scss'],
    animations: [routerTransition()],
    providers: [
        PublicidadService,
        ClasificacionService,
        UploadFileService]
})

export class PublicidadComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Publicidad';

    public error = false;
    public loading = false;
    submitted = false;
    public selectedID: number;
    public selectedEntity: Publicidad;
    public fecInicio: {};
    public fecFin: {};
    public dataClasificaciones: Array<any> = Array<any>();

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modalService: NgbModal,
        private publicidadService: PublicidadService,
        private clasificacionService: ClasificacionService,
        private uploadService: UploadFileService) {
        this.selectedEntity = new Publicidad();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.loading = true;
            this.selectedID = +params['id'];

            this.getData();
        });
    }

    public getData() {
        const p0 = this.clasificacionService.getList();
        Promise.all([
            p0
        ]).then(([result0]) => {
            this.dataClasificaciones = result0;
            this.setData();
        }).catch(e => this.handleError(e));
    }

    public getClasificaciones(): any {
        this.clasificacionService.getList()
        .then(
            response => {
                this.loading = false;
                this.dataClasificaciones = response as Clasificacion[];
            })
        .catch(e => this.handleError(e));
    }

    public setData(): any {
      if (this.selectedID > -1) {
        // this.selectedID = -1;
        this.publicidadService.getByID(this.selectedID)
        .then(
            response => {
                this.loading = false;
                this.selectedEntity = response as Publicidad;
                this.fecInicio = TWUtils.stringDateToJson(this.selectedEntity.fecinicio);
                this.fecFin = TWUtils.stringDateToJson(this.selectedEntity.fecfin);
                this.loading = false;
            })
        .catch(e => this.handleError(e));
      }
    }

    fileChange(event) {
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            this.uploadService.upload(file).then(
                response => {
                    this.selectedEntity.url = response.generatedName;
                }).catch(e => this.handleError(e));
        }
    }

    public onSubmit(content) {
        this.selectedEntity.fecinicio = TWUtils.arrayDateToString(this.fecInicio);
        this.selectedEntity.fecfin = TWUtils.arrayDateToString(this.fecFin);
        this.publicidadService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.loading = false;
                    this.selectedID = Number(response);
                    this.showMessage(content, 'La publicidad se guardó correctamente', this.MESSAGE_GOTO);
                } else {
                    this.error = true;
                    this.loading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar(content) {
        this.publicidadService.delete(this.selectedEntity).then(
            response => {
                if (response) {
                    this.loading = false;
                    this.selectedID = Number(response);
                    this.showMessage(content, 'La publicidad se ha eliminado!!', this.MESSAGE_GOTO);
                } else {
                    this.error = true;
                    this.loading = false;
                }
            }
        ).catch(e => this.handleError(e));

        console.log('eliminar');
    }

    public isNew() {
        return (this.selectedID === -1);
    }

    showMessage(content, message, action) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
            if (action === this.MESSAGE_GOTO) {
                this.router.navigate(['/publicidades']);
            }
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        alert('Permio guardado correctamente');
        return Promise.reject(error.message || error);
    }
}
