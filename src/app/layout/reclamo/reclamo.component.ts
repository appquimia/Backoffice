import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';

import { Reclamo, ReclamoService } from '../../shared/services';
import { ReclamoEstadoService } from '../../shared/services';

@Component({
    selector: 'app-form',
    templateUrl: './reclamo.component.html',
    styleUrls: ['./reclamo.component.scss'],
    animations: [routerTransition()],
    providers: [
        ReclamoService,
        ReclamoEstadoService]
})
export class ReclamoComponent implements OnInit {
    public error = false;
    public loading = false;
    public selectedID: string;
    public selectedReclamo: Reclamo;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private reclamoService: ReclamoService) {
        this.selectedReclamo = new Reclamo();
    }

    public ngOnInit(): void {
        this.loading = false;
        this.route.params.subscribe(params => {
            this.selectedID = params['id'];
            this.getReclamo();
        });
    }

    getReclamo() {
        if (this.selectedID !== '-1') {
            this.reclamoService.getByID(this.selectedID).then(
                response => {
                    this.loading = false;
                    this.selectedReclamo = response as Reclamo;
                }
            ).catch(e => this.handleError(e));
        }
    }

    nuevoEvento() {
        this.router.navigate(['/evento', -1]);
    }

    public isNew() {
        return (this.selectedID === '-1');
    }

    public eliminar() {
        console.log('eliminar');
    }

    volver() {
        this.router.navigate(['/reclamos']);
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
