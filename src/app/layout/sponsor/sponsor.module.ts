import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SponsorRoutingModule } from './sponsor-routing.module';
import { SponsorComponent } from './sponsor.component';
import { PageHeaderModule } from './../../shared';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import {
    DataComponent,
    ContactosComponent,
    ContactoComponent,
    ProductosComponent,
    ProductoComponent,
    SucursalesComponent,
    SucursalComponent } from './components';

@NgModule({
    imports: [
        CommonModule,
        SponsorRoutingModule,
        PageHeaderModule,
        FormsModule,
        NgbModule.forRoot(),
        ReactiveFormsModule,
        Ng2SmartTableModule
    ],
    declarations: [
        SponsorComponent,
        DataComponent,
        ContactosComponent,
        ContactoComponent,
        ProductosComponent,
        ProductoComponent,
        SucursalesComponent,
        SucursalComponent ]
})
export class SponsorModule { }
