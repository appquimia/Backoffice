
export class TWUtils {

    // static stringDateToJson(strindDate: any): any {
    //     if (strindDate !== null) {
    //         const datestring = strindDate.split('-');
    //         const newDate = new Date(+datestring[0], (+datestring[1]) - 1, +datestring[2], 0, 0, 0, 0);

    //         if (newDate.toString() !== 'Invalid Date' && strindDate !== '0000-00-00') {
    //             return newDate;
    //         } else { return '-1'; }
    //     } else { return '-1'; }
    // }

    static arrayDateToString(arrDate: any): any {
        if (arrDate['year'] !== undefined) {
            const selDate = arrDate['year'] + '-' + arrDate['month'] + '-' + arrDate['day'];
            return selDate;
        } else { return ''; }
    }

    static arrayDateToStringFormated(arrDate: any): any {
        if (arrDate['year'] !== undefined) {
            const selDate = arrDate['day'] + '-' + arrDate['month'] + '-' + arrDate['year'];
            return selDate;
        } else { return ''; }
    }

    static stringDateToJson(strindDate: any): any {
        const datestring = strindDate.split('-');
        const newDate = new Date(+datestring[0], (+datestring[1]) - 1, +datestring[2], 0, 0, 0, 0);
        return {
             'year': newDate.getFullYear(),
             'month': (newDate.getMonth() + 1),
             'day': newDate.getDate() };
    }

    static padLeft(text: string, padChar: string, size: number): string {
        return (String(padChar).repeat(size) + text).substr( (size * -1), size) ;
    }
}
