import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfiguracionTiempoComponent } from './configuraciontiempo.component';

const routes: Routes = [
    { path: '', component: ConfiguracionTiempoComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConfiguracionTiempoRoutingModule { }
