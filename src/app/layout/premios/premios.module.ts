import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { PaginationModule } from 'ng2-bootstrap';
import { Ng2TableModule } from 'ng2-table';

import { PremiosRoutingModule } from './premios-routing.module';
import { PremiosComponent } from './premios.component';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        FormsModule,
        Ng2TableModule,
        PaginationModule.forRoot(),
        CommonModule,
        PremiosRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [PremiosComponent]
})
export class PremiosModule { }
