import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

// import { Configuration } from './configuration';

export class TarifaPlantilla {
  public id: number;
  public fecha: string;
  public valor: string;
  public kms: string;
  constructor() {
    this.fecha = '';
    this.valor = '';
    this.kms = '';
  }
}

export class TarifaFijaPlantilla {
  public id: number;
  public fecha: string;
  public destino: string;
  public precio: string;
  constructor() {
    this.fecha = '';
    this.destino = '';
    this.precio = '';
  }
}

export class EsperaPlantilla {
  public id: number;
  public costo: string;
  constructor() {
    this.costo = '';
  }
}

@Injectable()
export class TarifaPlantillaService {
  readonly GET_TARIFAS = 'tarifas';
  readonly GET_FIJAS = 'fijas';
  readonly GET_ESPERA = 'espera';
  readonly PUT_TARIFAS = 'tarifas';
  readonly PUT_FIJAS= 'fijas';
  readonly PUT_ESPERA = 'espera';
  readonly PUT_APLICAR = 'aplicar';

  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'tarifaplantilla';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    this.headers.append('Pragma', 'no-cache');
  }

  getTarifas(): Promise<TarifaPlantilla[]> {
    const customUrl = `${this.apiUrl}/${this.GET_TARIFAS}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as TarifaPlantilla[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getTarifasFijas(): Promise<TarifaFijaPlantilla[]> {
    const customUrl = `${this.apiUrl}/${this.GET_FIJAS}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as TarifaFijaPlantilla[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getTiemposEsperas(): Promise<EsperaPlantilla[]> {
    const customUrl = `${this.apiUrl}/${this.GET_ESPERA}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as EsperaPlantilla[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  saveTarifa(entity: TarifaPlantilla): Promise<TarifaPlantilla> {
    if (entity.id) {
      return this.putTarifa(entity);
    }
    return null;
  }

  saveFija(entity: TarifaFijaPlantilla): Promise<TarifaFijaPlantilla> {
    if (entity.id) {
      return this.putFija(entity);
    }
    return null;
  }


  saveEspera(entity: EsperaPlantilla): Promise<EsperaPlantilla> {
    if (entity.id) {
      return this.putEspera(entity);
    }
    return null;
  }


  private putTarifa(entity: TarifaPlantilla): Promise<TarifaPlantilla> {
    const url = `${this.apiUrl}/${this.PUT_TARIFAS}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private putFija(entity: TarifaFijaPlantilla): Promise<TarifaFijaPlantilla> {
    const url = `${this.apiUrl}/${this.PUT_FIJAS}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private putEspera(entity: EsperaPlantilla): Promise<EsperaPlantilla> {
    const url = `${this.apiUrl}/${this.PUT_ESPERA}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  public aplicar(): Promise<boolean> {
    const url = `${this.apiUrl}/${this.PUT_APLICAR}`;

    return this.http
      .put(url, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
