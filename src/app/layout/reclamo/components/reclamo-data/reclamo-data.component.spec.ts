import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReclamoDataComponent } from './reclamo-data.component';

describe('ReclamoDataComponent', () => {
  let component: ReclamoDataComponent;
  let fixture: ComponentFixture<ReclamoDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot()
    ],
      declarations: [ ReclamoDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReclamoDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
