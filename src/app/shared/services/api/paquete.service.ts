import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

// import { Configuration } from './configuration';

export class Paquete {
  public id: number;
  public paquete: string;
}

@Injectable()
export class PaqueteService {
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {// , private configuration: Configuration) {
    // this.apiUrl = configuration.ServerWithApiUrl + 'paquete';
    this.apiUrl = environment.apiUrl + 'paquete';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<Paquete[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Paquete[];
        return entities;
        }
      ).catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<Paquete> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Paquete;
          return entity;
        }
      ).catch(e => this.handleError(e));
  }

  save(entity: Paquete): Promise<Paquete> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Paquete): Promise<Paquete> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Paquete): Promise<Paquete> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      ).catch(e => this.handleError(e));
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
